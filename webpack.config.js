const webpack = require("webpack");
const webpackCopy = require("copy-webpack-plugin");
const webpackTerser = require("terser-webpack-plugin");
const webpackReplace = require("replace-in-file-webpack-plugin");
const webpackExtract = require("mini-css-extract-plugin");
const webpackShell = require("webpack-shell-plugin-next");
const path = require("path");
const package = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

require("dotenv").config();

const config = (type) => {
    return {
        entry: `./src/${type}/${type}.ts`,
        output: {
            filename: `wp-tripetto${type !== "admin" ? "-" + type : ""}.js`,
            path: path.resolve(__dirname, "dist", "js"),
            libraryTarget: "umd",
            library: `WPTripetto${type !== "admin" ? type.substr(0, 1).toUpperCase() + type.substr(1) : ""}`,
            umdNamedDefine: true,
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig.json",
                        compilerOptions: {
                            noEmit: false,
                        },
                    },
                },
                {
                    test: /\.svg$/,
                    use: [
                        "url-loader",
                        {
                            loader: "image-webpack-loader",
                            options: {
                                svgo: {
                                    plugins: [
                                        { cleanupAttrs: true },
                                        { removeDoctype: true },
                                        { removeXMLProcInst: true },
                                        { removeComments: true },
                                        { removeMetadata: true },
                                        { removeTitle: true },
                                        {
                                            removeDesc: {
                                                removeAny: true,
                                            },
                                        },
                                    ],
                                },
                            },
                        },
                    ],
                },
                ...(type === "admin"
                    ? [
                          {
                              test: /\.s[ac]ss$/i,
                              use: [
                                  {
                                      loader: webpackExtract.loader,
                                  },
                                  {
                                      loader: "css-loader",
                                  },
                                  {
                                      loader: "sass-loader",
                                  },
                              ],
                          },
                          {
                              test: /\.(woff|woff2|eot|ttf)$/,
                              use: [
                                  {
                                      loader: "url-loader",
                                  },
                              ],
                          },
                      ]
                    : []),
            ],
        },
        resolve: {
            extensions: [".ts", ".js"],
        },
        externals: {
            tripetto: "Tripetto",
            "tripetto-runner-foundation": "TripettoRunner",
            "tripetto-runner-autoscroll": "TripettoAutoscroll",
            "tripetto-runner-chat": "TripettoChat",
            "tripetto-runner-classic": "TripettoClassic",
        },
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        output: {
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
                new webpack.BannerPlugin(`${package.title} ${package.version}`),
            ],
        },
        plugins: [
            new webpack.ProvidePlugin({
                Promise: "es6-promise-promise",
            }),
            ...(type === "admin"
                ? [
                      new webpackShell({
                          onBuildStart: {
                              scripts: ["npm run l10n"],
                              blocking: true,
                              parallel: false,
                          },
                      }),
                      new webpackExtract({
                          filename: `../css/wp-tripetto.css`,
                      }),
                      new webpackCopy({
                          patterns: [
                              {
                                  from: "./src/**/*.+(php|txt|png|svg)",
                                  to: "..",
                                  transformPath(targetPath) {
                                      return targetPath.replace("src\\", "").replace("src/", "");
                                  },
                              },
                              {
                                  from: "./dependencies/freemius",
                                  to: "../freemius",
                                  globOptions: {
                                      ignore: [
                                          "**/*.yml",
                                          "**/.github/**",
                                          "**/.git*",
                                          "**/gulpfile.js",
                                          "**/composer.json",
                                          "**/package.json",
                                      ],
                                  },
                              },
                              {
                                  from: "./translations/",
                                  to: "../languages/",
                              },
                              {
                                  from: "node_modules/tripetto/fonts/",
                                  to: "../fonts/",
                              },
                              {
                                  from: "node_modules/tripetto/locales/",
                                  to: "../locales/",
                              },
                              {
                                  from: "node_modules/tripetto/runtime/tripetto-umd.js",
                                  to: "../vendors/tripetto-builder.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-foundation/dist/umd/index.js",
                                  to: "../vendors/tripetto-runner.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-autoscroll/runner/index.js",
                                  to: "../vendors/tripetto-runner-autoscroll.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-chat/runner/index.js",
                                  to: "../vendors/tripetto-runner-chat.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-classic/runner/index.js",
                                  to: "../vendors/tripetto-runner-classic.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-autoscroll/builder/index.js",
                                  to: "../vendors/tripetto-builder-autoscroll.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-chat/builder/index.js",
                                  to: "../vendors/tripetto-builder-chat.js",
                              },
                              {
                                  from: "node_modules/tripetto-runner-classic/builder/index.js",
                                  to: "../vendors/tripetto-builder-classic.js",
                              },
                          ],
                      }),
                      new webpackReplace([
                          {
                              dir: "dist",
                              files: ["plugin.php", "readme.txt", "lib/database.php"],
                              rules: [
                                  {
                                      search: "{{ NAME }}",
                                      replace: "Tripetto",
                                  },
                                  {
                                      search: "{{ TITLE }}",
                                      replace: package.title,
                                  },
                                  {
                                      search: "{{ DESCRIPTION }}",
                                      replace: package.description,
                                  },
                                  {
                                      search: /\{\{\sVERSION\s\}\}/gi,
                                      replace: package.version,
                                  },
                                  {
                                      search: "{{ HOMEPAGE }}",
                                      replace: package.homepage,
                                  },
                                  {
                                      search: "{{ KEYWORDS }}",
                                      replace: (package.keywords || []).join(", "),
                                  },
                                  {
                                      search: "'secret_key' => ''",
                                      replace: `'secret_key' => '${process.env.FREEMIUS_SECRET_KEY || ""}'`,
                                  },
                              ],
                          },
                      ]),
                  ]
                : []),
            ...(production
                ? [
                      new analyzer({
                          analyzerMode: "static",
                          reportFilename: `../../reports/bundle-${type}.html`,
                          openAnalyzer: false,
                      }),
                  ]
                : []),
        ],
    };
};

module.exports = () => {
    return [config("admin"), config("builder"), config("runner")];
};
