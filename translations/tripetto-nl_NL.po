# Copyright (C) 2020 Tripetto
# This file is distributed under the same license as the Tripetto plugin.
msgid ""
msgstr ""
"Project-Id-Version: Tripetto 2.2.5\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/tripetto\n"
"POT-Creation-Date: 2020-10-05T20:37:40+00:00\n"
"PO-Revision-Date: 2020-10-05 22:38+0200\n"
"Last-Translator: Mark van den Brink <mark@vandenbr.ink>\n"
"Language-Team: \n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.1\n"
"X-Domain: tripetto\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "100%"
msgstr "100%"

msgid ""
"A premium subscription is required to be able to use all the automation "
"features."
msgstr ""
"Een premium abonnement is vereist om automatisering te kunnen gebruiken."

msgid "All without logic"
msgstr "Alles zonder logica"

msgid "Allow pausing and resuming"
msgstr "Sta pauzeren en hervatten toe"

msgid ""
"Allows users to pause the form and continue with it later by sending a "
"resume link to the user's email address."
msgstr ""
"Hiermee kunnen gebruikers het formulier pauzeren en later doorgaan middels "
"een koppeling die per e-mail wordt verzonden."

msgid "Are you sure you want to reset the styles for this form?"
msgstr "Weet je zeker dat je de stijlen voor dit formulier wilt resetten?"

msgid "Automate"
msgstr "Automatiseer"

msgid "Autoscroll"
msgstr "Autoscroll"

msgid "Back"
msgstr "Vorige"

msgid ""
"Both the form and collected data are hosted and stored in _your own "
"WordPress instance_. Not a single connection related to Tripetto is ever "
"made with an external host other than yours. It's all in your own instance, "
"under your control. Hello [GDPR](%1)!"
msgstr ""
"Zowel het formulier als de verzamelde gegevens worden gehost en opgeslagen "
"in _uw eigen WordPress-installatie_. Er wordt door Tripetto nooit een "
"verbinding gemaakt met een externe host anders dan uw eigen host. Alles is "
"dus opgeslagen in uw eigen installatie en daarmee onder uw volledig "
"controle. Hallo [AVG](%1)!"

msgid "Build"
msgstr "Bouw"

msgid "Chat"
msgstr "Chat"

msgid "Cheatsheet"
msgstr "Spiekbriefje"

msgid "Classic"
msgstr "Klassiek"

msgid "Close"
msgstr "Sluiten"

msgid "Contact"
msgstr "Contact"

msgid "Contact us"
msgstr "Contact opnemen"

#: admin/forms/list.php:134
msgid "Copy to clipboard"
msgstr "Zet op klembord"

#: admin/forms/list.php:140
msgid "Customize"
msgstr "Personaliseer"

msgid "Design"
msgstr "Ontwerp"

msgid "Display form as full page overlay"
msgstr "Formulier weergeven als volledige pagina"

msgid ""
"Don't use this, unless you know what you are doing. We don't give support on "
"forms with custom CSS! If you have a problem with a form that uses custom "
"CSS, then first disable the custom CSS and check if the problem persists."
msgstr ""
"Gebruik dit alleen als je weet wat je doet. We geven geen enkele "
"ondersteuning op formulieren met aangepaste CSS regels! Als u een probleem "
"heeft met een formulier dat aangepaste CSS gebruikt, schakelt u dan eerst de "
"aangepaste CSS uit en controleer of het probleem blijft bestaan."

msgid "Examples"
msgstr "Voorbeelden"

msgid "Follow us"
msgstr "Volg ons"

msgid "Form appearance"
msgstr "Formulierweergave"

msgid "Form face"
msgstr "Formulieruiterlijk"

msgid "Help center"
msgstr "Helpcentrum"

msgid "Help with form faces"
msgstr "Help bij formulieruiterlijken"

msgid "Include response data in the message"
msgstr "Gegevens opnemen in het bericht"

msgid "More..."
msgstr "Meer…"

msgid "Navigation"
msgstr "Navigatie"

msgid "No"
msgstr "Nee"

msgid ""
"Notify an external service when someone completes your form ([learn more]"
"(%1))"
msgstr ""
"Een externe service op de hoogte stellen wanneer iemand uw formulier invult "
"([meer informatie](%1))"

msgid "Open in browser"
msgstr "Openen in browser"

msgid ""
"Please create an incoming [Slack webhook](https://api.slack.com/incoming-"
"webhooks) and enter the URL of the webhook here:"
msgstr ""
"Maak een binnenkomende [Slack webhook](https://api.slack.com/incoming-"
"webhooks) en voer hier de URL van de webhook in:"

msgid "Post the data to the following URL:"
msgstr "Stuur de gegevens naar de volgende URL:"

msgid "Properties"
msgstr "Eigenschappen"

msgid "Receive updates"
msgstr "Updates ontvangen"

msgid "Report issue/bug"
msgstr "Probleem/bug rapporteren"

msgid "Reset styles"
msgstr "Stijlen opnieuw instellen"

#: admin/forms/list.php:64 admin/results/results.php:75
msgid "Results"
msgstr "Resultaten"

#: admin/forms/list.php:58
msgid "Run"
msgstr "Uitvoeren"

msgid "Save and restore uncompleted forms"
msgstr "Onvoltooide formulieren opslaan en herstellen"

msgid ""
"Saves uncompleted forms in the local storage of the browser. Next time the "
"user visits the form it is restored so the user can continue."
msgstr ""
"Slaat onvoltooide formulieren op in de lokale opslag van de browser. De "
"volgende keer dat de gebruiker het formulier bezoekt, wordt het formulier "
"hersteld, zodat de gebruiker verder kan gaan."

msgid ""
"Send a Slack message when someone completes your form ([learn more](%1))"
msgstr ""
"Een Slack-bericht verzenden wanneer iemand uw formulier invult ([meer "
"informatie](%1))"

msgid "Send a notification to:"
msgstr "Stuur een melding naar:"

msgid "Send an email when someone completes your form ([learn more](%1))"
msgstr ""
"Een e-mail verzenden wanneer iemand uw formulier invult ([meer informatie]"
"(%1))"

msgid "Send raw response data to webhook"
msgstr "Ruwe gegevens naar webhook verzenden"

msgid "Share"
msgstr "Deel"

msgid "Specify a fixed height for the form"
msgstr "Een vaste hoogte voor het formulier opgeven"

msgid "Specify a fixed width for the form"
msgstr "Een vaste breedte voor het formulier opgeven"

msgid "Specify custom CSS styles"
msgstr "Aangepaste CSS-stijlen opgeven"

msgid "Styles"
msgstr "Stijlen"

#: admin/dashboard/dashboard.php:88
msgid "Support"
msgstr "Ondersteuning"

msgid "Test with logic"
msgstr "Testen met logica"

msgid ""
"This option sends way more data to your webhook. Unfortunately most webhooks "
"(like Zapier, Integromat, etc.) don't like that and want plain name-value "
"pairs. In that case you should keep this option disabled."
msgstr ""
"Met deze optie worden veel meer gegevens naar uw webhook gestuurd. Helaas "
"willen de meeste webhooks (zoals Zapier, Integromat, enz.) gewoon naam-"
"waarde paren. In dat geval moet u deze optie uitgeschakeld houden."

msgid "To specify rules for a specific block, use this selector: %1"
msgstr ""
"Als u regels voor een specifiek blok wilt opgeven, gebruikt u deze regel: %1"

msgid "Translations"
msgstr "Vertalingen"

msgid "Tutorials"
msgstr "Handleidingen"

msgid "Unnamed"
msgstr "Naamloos"

msgid ""
"Use the shortcode below to embed the form on any page on your website "
"([learn more](%1))."
msgstr ""
"Gebruik onderstaande shortcode om het formulier in te sluiten op een pagina "
"op uw website ([meer informatie](%1))."

msgid "Yes, reset it!"
msgstr "Ja, stel opnieuw in!"

msgid ""
"Your actions aren't being processed properly and probably not saved to the "
"server. We apologize for the inconvenience. Please try again and drop a line "
"if troubles persist."
msgstr ""
"Uw acties worden niet goed verwerkt en worden waarschijnlijk niet opgeslagen "
"op de server. Onze excuses voor het ongemak. Probeer het opnieuw en laat het "
"ons weten als de problemen aanhouden."

msgid ""
"Your form is automatically available through a dedicated link ([learn more]"
"(%1))."
msgstr ""
"Uw formulier is automatisch beschikbaar via een directe link ([meer "
"informatie](%1))."

msgid "auto"
msgstr "auto"

msgid "⏳ Testing..."
msgstr "⏳ Testen..."

msgid "✌ Our promise on data privacy"
msgstr "✌ Onze belofte over gegevensprivacy"

msgid "✔ All good!"
msgstr "✔ In orde!"

msgid "✔ Copied!"
msgstr "✔ Gekopieerd!"

msgid "❌ Something's wrong"
msgstr "❌ Er is iets mis"

msgid "👨‍💻 WordPress shortcode"
msgstr "👨‍💻 WordPress shortcode"

msgid "📣 Slack notification"
msgstr "📣 Slack-melding"

msgid "📧 Email notification"
msgstr "📧 E-mailmelding"

msgid "🔗 Webhook"
msgstr "🔗 Webhook"

msgid "🤯 Something’s cracking"
msgstr "🤯 Er gaat iets mis"

msgid "🧭 Shareable link"
msgstr "🧭 Deelbare koppeling"

msgid "🩺 Test"
msgstr "🩺 Test"

#. Plugin Name of the plugin
#. Author of the plugin
#: admin/admin.php:35 admin/admin.php:36
msgid "Tripetto"
msgstr "Tripetto"

#. Plugin URI of the plugin
msgid "https://tripetto.com/wordpress"
msgstr ""

#. Description of the plugin
#: admin/dashboard/dashboard.php:25
msgid ""
"Give life to forms and surveys. We merged the best of WP Forms, Typeform and "
"Landbot into a single full-fledged plugin for conversational experiences."
msgstr ""
"Breng uw formulier tot leven. We hebben het beste van WP Forms, Typeform en "
"Landbot gecombineerd tot één complete plugin voor het maken van interactieve "
"gesprekken."

#: admin/admin.php:27
msgid "You cannot access this page. You are not authorized."
msgstr "U heeft geen toegang tot deze pagina. U bent niet bevoegd."

#: admin/builder/builder.php:57
msgid "Creating new form..."
msgstr "Nieuw formulier wordt aangemaakt..."

#. translators: %s contains the error message
#: admin/builder/builder.php:64
msgid "Something went wrong, could not create a new form (%s)."
msgstr "Er ging iets mis, kon geen nieuw formulier maken (%s)."

#: admin/builder/builder.php:145
msgid "One moment please..."
msgstr "Een ogenblik geduld alstublieft..."

#: admin/builder/builder.php:160
msgid "Something went wrong, could not fetch the form."
msgstr "Er ging iets mis, kon het formulier niet ophalen."

#: admin/builder/builder.php:327
msgid "This is a test message from Tripetto. Your Slack configuration works ✔"
msgstr "Dit is een testbericht van Tripetto. Uw Slack-configuratie werkt ✔"

#: admin/builder/builder.php:336 admin/builder/builder.php:386
msgid "Test"
msgstr "Test"

#: admin/builder/builder.php:337 admin/builder/builder.php:381
#: admin/builder/builder.php:387 admin/builder/builder.php:388
msgid "Hello World!"
msgstr "Hallo wereld!"

#: admin/dashboard/dashboard.php:24
msgid "Welcome to Tripetto!"
msgstr "Welkom bij Tripetto!"

#: admin/dashboard/dashboard.php:28
msgid "Get Started"
msgstr "Aan de slag"

#: admin/dashboard/dashboard.php:30
msgid "Create your form using the unique visual form builder"
msgstr "Uw formulier maken met behulp van de unieke visuele formulierbouwer"

#: admin/dashboard/dashboard.php:31
msgid "See a realtime preview of your form, on different devices"
msgstr ""
"Bekijk een actuele voorbeeldweergave van uw formulier op verschillende "
"apparaten"

#: admin/dashboard/dashboard.php:32
msgid "Style your form and remove branding"
msgstr "Uw formulier stijlen en reclame verwijderen"

#: admin/dashboard/dashboard.php:32 admin/dashboard/dashboard.php:50
#: admin/dashboard/dashboard.php:51
msgid "Premium"
msgstr "Premium"

#: admin/dashboard/dashboard.php:37
msgid "Publish your form"
msgstr "Uw formulier publiceren"

#: admin/dashboard/dashboard.php:39
msgid "Share your form using a direct link"
msgstr "Deel uw formulier middels een link"

#: admin/dashboard/dashboard.php:40
msgid "Or embed on any page using a shortcode"
msgstr "Of embed op een pagina middels de shortcode"

#. translators: %s is replaced with the word `here` (that links to a help page)
#: admin/dashboard/dashboard.php:43
msgid "Learn more about publishing your forms %s"
msgstr "Lees %s meer over het publiceren van formulieren"

#. translators: %s is replaced with the word `here` (that links to a help page)
#: admin/dashboard/dashboard.php:43
msgid "here"
msgstr "hier"

#: admin/dashboard/dashboard.php:47
msgid "Collect results and get notified"
msgstr "Resultaten verzamelen en notificaties ontvangen"

#: admin/dashboard/dashboard.php:49
msgid "View results and export to CSV"
msgstr "Resultaten weergeven en exporteren naar CSV"

#: admin/dashboard/dashboard.php:50
msgid "Push results to Slack"
msgstr "Stuur resultaten naar Slack"

#: admin/dashboard/dashboard.php:51
msgid "Push results to webhooks (Zapier, Integromat)"
msgstr "Stuur resultaten naar webhooks (Zapier, Integromat)"

#: admin/dashboard/dashboard.php:57
msgid "View All Forms"
msgstr "Alle formulieren bekijken"

#: admin/dashboard/dashboard.php:58 admin/forms/forms.php:111
msgid "Create New Form"
msgstr "Nieuw formulier aanmaken"

#: admin/dashboard/dashboard.php:59
msgid "Start creating your first Tripetto form!"
msgstr "Maak uw eerste Tripetto formulier!"

#. translators: %1$s is replaced with the word `tutorials`, %2$d is replaced with `examples` and %3$s with `help center`
#: admin/dashboard/dashboard.php:62
msgid "Or browse our %1$s and %2$s or visit the %3$s if you need help."
msgstr ""
"Of blader door onze %1$s en %2$s of bezoek het %3$s als u hulp nodig heeft."

#. translators: %1$s is replaced with the word `tutorials`, %2$d is replaced with `examples` and %3$s with `help center`
#: admin/dashboard/dashboard.php:62
msgid "tutorials"
msgstr "handleidingen"

#. translators: %1$s is replaced with the word `tutorials`, %2$d is replaced with `examples` and %3$s with `help center`
#: admin/dashboard/dashboard.php:62
msgid "examples"
msgstr "voorbeelden"

#. translators: %1$s is replaced with the word `tutorials`, %2$d is replaced with `examples` and %3$s with `help center`
#: admin/dashboard/dashboard.php:62
msgid "help center"
msgstr "helpcentrum"

#: admin/dashboard/dashboard.php:71
msgid "Statistics"
msgstr "Statistieken"

#: admin/dashboard/dashboard.php:76
msgid "# Forms"
msgstr "# Formulieren"

#: admin/dashboard/dashboard.php:80 admin/forms/list.php:189
msgid "# Results"
msgstr "# Resultaten"

#: admin/dashboard/dashboard.php:93
msgid "Need help?"
msgstr "Hulp nodig?"

#: admin/dashboard/dashboard.php:95
msgid "Visit our Help Center"
msgstr "Bezoek ons Help Center"

#: admin/dashboard/dashboard.php:99
msgid "Changelog"
msgstr "Veranderingen"

#: admin/dashboard/dashboard.php:101
msgid "What's new in v"
msgstr "Wat is er nieuw in v"

#: admin/dashboard/dashboard.php:107
msgid "Got a question?"
msgstr "Heeft u een vraag?"

#: admin/dashboard/dashboard.php:109
msgid "Ask for support"
msgstr "Vraag om ondersteuning"

#: admin/dashboard/dashboard.php:113
msgid "Found a bug?"
msgstr "Bug gevonden?"

#: admin/dashboard/dashboard.php:115
msgid "Submit an issue"
msgstr "Een probleem melden"

#: admin/dashboard/dashboard.php:127
msgid "Premium features"
msgstr "Premium functies"

#: admin/dashboard/dashboard.php:130
msgid "Upgrade to premium and unlock the following extras for all your forms:"
msgstr ""
"Upgrade naar premium en ontgrendel de volgende extra's voor al je "
"formulieren:"

#: admin/dashboard/dashboard.php:132
msgid "Remove Tripetto branding"
msgstr "Tripetto-reclame verwijderen"

#: admin/dashboard/dashboard.php:133
msgid "Receive responses in Slack"
msgstr "Ontvang inzendingen in Slack"

#: admin/dashboard/dashboard.php:134
msgid "Connect with webhooks (Zapier, Integromat, etc.)"
msgstr "Maak verbinding met webhooks (Zapier, Integromat, enz.)"

#: admin/dashboard/dashboard.php:136
msgid "Upgrade To Premium"
msgstr "Upgraden naar Premium"

#: admin/dashboard/dashboard.php:136
msgid "Thanks for upgrading!"
msgstr "Bedankt voor het upgraden!"

#: admin/dashboard/dashboard.php:145
msgid "Stay tuned - Twitter"
msgstr "Volg ons op Twitter"

#: admin/forms/forms.php:47
msgid "All Forms"
msgstr "Alle formulieren"

#: admin/forms/forms.php:54 admin/forms/forms.php:55
msgid "Add New"
msgstr "Voeg nieuwe toe"

#: admin/forms/forms.php:99
msgid "Form deleted!"
msgstr "Formulier verwijderd!"

#: admin/forms/forms.php:103
msgid "Form duplicated!"
msgstr "Formulier gedupliceerd!"

#: admin/forms/forms.php:107
msgid "Tripetto Forms"
msgstr "Tripetto Formulieren"

#: admin/forms/list.php:13
msgid "form"
msgstr "formulier"

#: admin/forms/list.php:14
msgid "forms"
msgstr "formulieren"

#: admin/forms/list.php:52
msgid "Edit"
msgstr "Bewerk"

#: admin/forms/list.php:91
msgid "Duplicate"
msgstr "Dupliceer"

#: admin/forms/list.php:101 admin/results/list.php:36
#: admin/results/results.php:234
msgid "Confirm delete"
msgstr "Bevestig verwijdering"

#: admin/forms/list.php:102
msgid ""
"All the data and the form itself will be removed. This action cannot be made "
"undone."
msgstr ""
"Alle gegevens en het formulier zelf worden verwijderd. Deze actie kan niet "
"ongedaan worden gemaakt."

#: admin/forms/list.php:103 admin/results/list.php:38
#: admin/results/results.php:238
msgid "Yes, delete it"
msgstr "Ja, verwijderen"

#: admin/forms/list.php:104 admin/results/list.php:39
#: admin/results/results.php:240
msgid "No, keep it"
msgstr "Nee, bewaren"

#: admin/forms/list.php:108 admin/results/list.php:43
#: admin/results/list.php:105
msgid "Delete"
msgstr "Verwijder"

#: admin/forms/list.php:188
msgid "Name"
msgstr "Naam"

#: admin/forms/list.php:190
msgid "Created"
msgstr "Aangemaakt"

#: admin/forms/list.php:191
msgid "Modified"
msgstr "Gewijzigd"

#: admin/forms/list.php:192
msgid "Shortcode"
msgstr "Shortcode"

#: admin/results/export.php:42 admin/results/list.php:77
#: admin/results/results.php:223
msgid "Date submitted"
msgstr "Datum ingediend"

#: admin/results/export.php:42 admin/results/list.php:75
msgid "# Number"
msgstr "# Nummer"

#: admin/results/export.php:42
msgid "Reference"
msgstr "Referentie"

#: admin/results/list.php:9
msgid "result"
msgstr "resultaat"

#: admin/results/list.php:10 admin/results/results.php:73
msgid "results"
msgstr "resultaten"

#: admin/results/list.php:32
msgid "View Result"
msgstr "Resultaat weergeven"

#: admin/results/list.php:37 admin/results/results.php:236
msgid "Are you sure you want to delete the result?"
msgstr "Weet u zeker dat u het resultaat wilt verwijderen?"

#: admin/results/list.php:76 admin/results/results.php:228
msgid "Identifier"
msgstr "Identificatie"

#. translators: %d is replaced with the number of results deleted
#: admin/results/results.php:63
msgid "Results deleted: %d"
msgstr "Resultaten verwijderd: %d"

#: admin/results/results.php:79
msgid "Back to forms list"
msgstr "Terug naar formulieren"

#: admin/results/results.php:83
msgid "Edit form"
msgstr "Bewerk formulier"

#: admin/results/results.php:97 admin/results/results.php:116
msgid "Export results as CSV"
msgstr "Exporteren naar CSV"

#: admin/results/results.php:109
msgid "Version"
msgstr "Versie"

#: admin/results/results.php:119
msgid "Why do I see multiple CSV versions?"
msgstr "Waarom zie ik meerdere CSV-versies?"

#: admin/results/results.php:137
msgid "Something went wrong, could not fetch the results."
msgstr "Er ging iets mis, kon de resultaten niet ophalen."

#: admin/results/results.php:162
msgid "Result"
msgstr "Resultaat"

#: admin/results/results.php:169
msgid "Back to results"
msgstr "Terug naar resultaten"

#: admin/results/results.php:207
msgid "Information"
msgstr "Informatie"

#: admin/results/results.php:211
msgid "Form name"
msgstr "Formuliernaam"

#: admin/results/results.php:218
msgid "Number"
msgstr "Nummer"

#: admin/results/results.php:246
msgid "Delete Result"
msgstr "Resultaat verwijderen"

#: admin/results/results.php:263
msgid "Something went wrong, could not fetch the result."
msgstr "Er ging iets mis, kon het resultaat niet ophalen."

#. translators: %s is replaced with the current version number of Tripetto
#: lib/installation.php:67
msgid "Welcome to Tripetto version %s"
msgstr "Welkom bij Tripetto versie %s"

#: lib/installation.php:69
msgid "Thank you for upgrading!"
msgstr "Bedankt voor het upgraden!"

#: lib/installation.php:71
msgid "Want to know what's new?"
msgstr "Wilt u weten wat er nieuw is?"

#: lib/installation.php:73
msgid "Check out our changelog"
msgstr "Bekijk de lijst met veranderingen"

#: lib/installation.php:76
msgid "Want to help improve the product and spread the word?"
msgstr ""
"Wilt u ons helpen Tripetto beter te maken en de bekendheid ervan te "
"vergroten?"

#: lib/installation.php:78
msgid "Rate/review us on WordPress"
msgstr "Beoordeel ons op WordPress"

#: lib/license.php:72
msgid "Important message for you"
msgstr "Belangrijk bericht voor u"

#: lib/license.php:74
msgid ""
"Please note that this latest free version of Tripetto does no longer come "
"with one free premium form for new users. But you are not a new user! You "
"were using Tripetto before this big upgrade and we are very grateful for "
"that. That’s why the form you designated as premium form will keep all its "
"premium features going forward. You can recognize it by the premium badge in "
"your list of forms."
msgstr ""
"In deze nieuwe versie van Tripetto hebben we de mogelijkheid om een gratis "
"premium formulier aan te maken verwijderd. Dit geldt voor nieuwe gebruikers, "
"maar niet voor bestaande gebruikers zoals u. Omdat u al gebruik maakte van "
"Tripetto vinden we het niet netjes als u opeens uw gratis premium formulier "
"zou kwijtraken. Daarom blijft voor u het gratis premium formulier "
"beschikbaar."

#: runner/runner.php:192
msgid ""
"Normally you should see a Tripetto form over here, but it needs JavaScript "
"to run properly and it seems that is disabled in your browser. Please enable "
"JavaScript to see and use the form."
msgstr ""
"Normaal gesproken zou u hier een Tripetto formulier zien. Alleen is hier "
"JavaScript ondersteuning voor nodig en het lijkt erop dat dat is "
"uitgeschakeld in uw browser. Om het formulier te kunnen zien en gebruiken, "
"dient u JavaScript in uw browser te activeren."

#: runner/runner.php:761
msgid "Unnamed form"
msgstr "Naamloos formulier"

#. translators: %1$s is replaced with the form name and %2$d is replaced with the form index
#: runner/runner.php:769
msgid "New submission from %1$s (#%2$d)"
msgstr "Nieuwe inzending van %1$s (#%2$d)"

#. translators: %1$d is replaced with the form index and %2$s is replaced with the form name
#: runner/runner.php:795
msgid "New submission `#%1$d` for `%2$s`"
msgstr "Nieuwe inzending '#%1$d' voor '%2$s'"

#: runner/runner.php:938
msgid "Resume your form with this magic link"
msgstr "Hervat uw formulier met deze magische link"

#: templates/footer.php:21
msgid "Sent to"
msgstr "Verstuurd naar"

#: templates/footer.php:23
msgid "by"
msgstr "door"

#: templates/footer.php:27
msgid "Get conversational!"
msgstr ""

#: templates/notification.php:15
msgid "New submission"
msgstr "Nieuwe inzending"

#: templates/notification.php:18
msgid "A new submission (#{{index}}) was received for form"
msgstr "Er is een nieuwe inzending (#{{index}}) ontvangen voor formulier"

#: templates/notification.php:19
msgid "View in WordPress"
msgstr "Bekijken in WordPress"

#: templates/snapshot.php:15
msgid "Form paused"
msgstr "Formulier gepauzeerd"

#: templates/snapshot.php:18
msgid "You took a break while filling out the form. Sweet!"
msgstr "Je nam een pauze tijdens het invullen van het formulier. Prima!"

#: templates/snapshot.php:20
msgid "Click the link below at any time to continue right where you left off."
msgstr "Klik op de onderstaande link om verder te gaan waar je was gebleven."

#: templates/snapshot.php:24
msgid "Resume your form"
msgstr "Uw formulier hervatten"

#~ msgid ""
#~ "Easily create smart forms and surveys. From a simple contact form to an "
#~ "advanced survey with lots of logic."
#~ msgstr ""
#~ "Maak eenvoudig slimme formulieren en enquêtes. Van een eenvoudig "
#~ "contactformulier tot een geavanceerde enquête met veel logica."

#~ msgid "Templates"
#~ msgstr "Sjablonen"

#~ msgid "Publish your form using the Tripetto shortcode"
#~ msgstr "Publiceer uw formulier met de Tripetto shortcode"

#~ msgid ""
#~ "See our %s for more information about using the shortcode and its "
#~ "parameters"
#~ msgstr ""
#~ "Zie onze %s voor meer informatie over het gebruik van de shortcode en de "
#~ "parameters"

#~ msgid "Get Started page"
#~ msgstr "Aan de slag-pagina"

#~ msgid "Or visit our %1$s for %2$s, %3$s and %4$s."
#~ msgstr "Of bezoek onze %1$s voor %2$s, %3$s en %4$s."

#~ msgid "instructions"
#~ msgstr "instructies"

#~ msgid "templates"
#~ msgstr "sjablonen"

#~ msgid "Resultaat"
#~ msgstr "Resultaat"

#~ msgid ""
#~ "Always free: One form with all premium features (without branding "
#~ "removal)."
#~ msgstr ""
#~ "Altijd gratis: Een formulier met alle premium functies (maar wel met "
#~ "reclame)."

#~ msgid "Form was successfully marked as Premium"
#~ msgstr "Formulier is nu Premium"

#~ msgid "Mark as Premium"
#~ msgstr "Maak Premium"

#~ msgid ""
#~ "You are about to designate this form as your complimentary Premium form. "
#~ "The premium settings for your current Premium form will be lost. Are you "
#~ "sure you want to switch?"
#~ msgstr ""
#~ "U staat op het punt dit formulier aan te wijzen als uw gratis Premium-"
#~ "formulier. De premium-instellingen voor uw huidige Premium-formulier gaan "
#~ "verloren. Weet u zeker dat u wilt wisselen?"

#~ msgid "Yes, switch it"
#~ msgstr "Ja, schakel om"

#~ msgid "No, stop"
#~ msgstr "Nee, stop"

#~ msgid "Customize shortcode"
#~ msgstr "Shortcode aanpassen"

#~ msgid "Embed"
#~ msgstr "Embed"

#~ msgid "Preview"
#~ msgstr "Voorbeeld"
