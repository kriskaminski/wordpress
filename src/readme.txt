=== {{ TITLE }} ===
Contributors: tripetto
Tags: {{ KEYWORDS }}
Requires at least: 4.9.10
Tested up to: 5.5.1
Requires PHP: 5.6.20
Stable tag: {{ VERSION }}
License: GPLv2 or later

{{ DESCRIPTION }}

== Description ==

**Tired of boring and ugly forms in your WordPress site?** 😴
**Use Tripetto to make your forms smart and conversational!** 😍
**And boost the completion rates of your online forms!** 🚀

- Full featured form creator plugin;
- Everything, including all data, inside your own WP Admin;
- Built-in anti-spam protection without the need for using CAPTCHAs (<a href="https://tripetto.com/help/articles/how-tripetto-prevents-spam-entries-from-your-forms/" rel="friend" title="How we fight spam" target="_blank">learn more</a>);
- All without a single line of code;
- No third-party account needed, not even a Tripetto account;
- GDPR proof.

🎉 <a href="https://tripetto.com/blog/our-biggest-update-ever-now-also-available-for-wordpress/" rel="friend" title="Blog article about version 3 launch" target="_blank">Read our blog to learn what's inside version 3 of this plugin!</a>

### 🧐 What makes Tripetto a better form plugin? ###

💡 You create stunning custom form experiences instead of boring forms. You can use all your forms in three form faces:

- **Autoscroll**: the autoscroll face presents one question at a time and is akin to Typeform’s conversational forms;
- **Chat**: the chat face presents all questions and answers as speech bubbles and is partly inspired by Landbot;
- **Classic**: the classic face presents question fields in a traditional form as often seen in SurveyMonkey and the likes.

<a href="https://tripetto.com/product/#customization" rel="friend" title="More info about form faces" target="_blank">More info about form faces over here.</a>

💡 You create smart forms that react to the given answers of your respondents. That’s why we call them conversational forms. To help you with this, our unique visual form builder easily lets you create the right logic, like **branch logic**, **jump logic** and **pipe logic**.

<a href="https://tripetto.com/product/#logic" rel="friend" title="More info about logic" target="_blank">More info about logic over here.</a>

💡 You create beautiful responsive forms that match your website perfectly, with custom **welcome and end screens**, advanced **styling options** and **full translations**. **Totally responsive** for mobile, tablet, laptop and desktop usage.

<a href="https://tripetto.com/product/#customization" rel="friend" title="More info about customizations" target="_blank">More info about customizations over here.</a>

👉 **<a href="https://tripetto.com/features/" rel="friend" title="See all features" target="_blank">See our full features overview here.</a>**

### 👷 Give shape - Easily build your forms ###
- Visually build on the assistive storyboard, unlike any other form builder;
- Real-time preview while building your form;
- All question types you need included. See FAQ section below for the full list;
- Add a custom welcome screen before the form starts;
- Add flexible custom end screens, or redirect at the end.

<a href="https://tripetto.com/help/wordpress/building-forms-and-surveys/" rel="friend" title="Learn more about building" target="_blank">Learn more about building.</a>

### ✨ Add brains - Make your forms smart ###
- Advanced logic features to make your form feel like a conversation;
- Various types of advanced form logic making it a conditional form;
- Branch logic, to only ask the right follow-up questions;
- Skip logic, to jump over unnessecary questions;
- Pipe logic, to show given answers inside your form.

<a href="https://tripetto.com/help/wordpress/using-logic-features/" rel="friend" title="Learn more about using logic" target="_blank">Learn more about using logic.</a>

### 🎨 Dress up - Customize your forms to your needs ###
- Choose between three form experiences: autoscroll, chat, or classic;
- Choose autoscroll for conversational forms, for example feedback forms, screening forms and large surveys;
- Choose chat for chat forms that feel like chatbots, for example support forms, evaluation forms (incl. Net Promoter Score/NPS) and RSVP forms;
- Choose classic for traditional looking but smart forms, for example contact forms, reservation forms and registration forms;
- Style your forms (fonts, colors, backgrounds, buttons, inputs);
- Translate/edit all labels inside your forms;
- All forms fully optimized for perfect responsiveness on mobile, tablet, laptop and desktop.

<a href="https://tripetto.com/help/wordpress/styling-and-customization/" rel="friend" title="Learn more about customization" target="_blank">Learn more about customization.</a>

### ⚡ Hook up - Automate things ###
- Automate email notifications upon form completion;
- Automate Slack notifications upon form completion (premium);
- Connect to webhooks (like Zapier, Integromat, etc.) to automate processes (premium).

<a href="https://tripetto.com/help/wordpress/automating-things/" rel="friend" title="Learn more about automating" target="_blank">Learn more about automating.</a>

### ⚡ Send out - Share how you wish ###
- Immediately share your form with the shareable link in your WP site, making your WP site a complete form tool/survey tool instantly;
- Or embed the form wherever you want with the WP shortcode;
- Compose your shortcode with the shortcode editor (no-code solution).

<a href="https://tripetto.com/help/wordpress/sharing-forms-and-surveys/" rel="friend" title="Learn more about sharing" target="_blank">Learn more about sharing.</a>

### 🛡️ Take stock - Handle responses safely ###
- Responses are stored in your own WordPress install only (no third-party storage);
- Built-in SPAM protection, so no needs for captcha’s etc.;
- View and manage results inside your WP Admin;
- Export to CSV.

<a href="https://tripetto.com/help/wordpress/managing-data-and-results/" rel="friend" title="Learn more about data management" target="_blank">Learn more about data management.</a>

### 👑 Premium features ###
Upgrade the Tripetto WordPress plugin to greatly enhance all your forms and surveys. Premium features are:

- Remove Tripetto branding;
- Automate Slack notifications;
- Connect to webhooks (like Zapier, Integromat, etc.).

Available as single-site, 5-sites and unlimited sites. Billed monthly or annually. Or pay once with a lifetime deal!

👉 **<a href="https://tripetto.com/pricing/wordpress/" rel="friend" title="Get your premium license" target="_blank">Get your Premium license today!</a>**

### 🕵️‍♂️ Compare Tripetto with others ###
We are the new kid on the block, so we understand that a comparison will help you decide if Tripetto is the right fit for you.

- <a href="https://tripetto.com/wpforms-alternative/" rel="friend" title="Tripetto is a WPForms alternative" target="_blank">Compare Tripetto with WPForms</a>
- <a href="https://tripetto.com/contactform7-alternative/" rel="friend" title="Tripetto is a Contact Form 7 alternative" target="_blank">Compare Tripetto with Contact Form 7</a>
- <a href="https://tripetto.com/ninjaforms-alternative/" rel="friend" title="Tripetto is a Ninja Forms alternative" target="_blank">Compare Tripetto with Ninja Forms</a>
- <a href="https://tripetto.com/gravityforms-alternative/" rel="friend" title="Tripetto is a Gravity Forms alternative" target="_blank">Compare Tripetto with Gravity Forms</a>
- <a href="https://tripetto.com/typeform-alternative/" rel="friend" title="Tripetto is a Typeform alternative" target="_blank">Compare Tripetto with Typeform</a>

👉 **<a href="https://tripetto.com/compare/" rel="friend" title="Compare Tripetto" target="_blank">Compare Tripetto with others.</a>**

### 🔔 Stay up-to-date ###
Any questions? We're happy to help!

- <a href="https://tripetto.com/contact/" rel="friend" title="Contact us" target="_blank">Contact us</a>
- <a href="https://tripetto.com/subscribe/" rel="friend" title="Subscribe to Tripetto newsletter" target="_blank">Subscribe to our newsletter</a>
- <a href="https://www.twitter.com/tripetto/" rel="friend" title="Follow Tripetto on Twitter" target="_blank">Follow us on Twitter</a>

== Installation ==

👉 Install Tripetto via the WordPress.org plugin repository or by uploading the files to your server;

👉 Activate the plugin through the Plugins page in your WP Admin;

👉 Navigate to the Tripetto tab in your WP Admin menu.

👑 Get your <a href="https://tripetto.com/pricing/wordpress/" rel="friend" target="_blank">Premium License</a> to upgrade all your forms!

== Frequently Asked Questions ==

= Can I use Tripetto for free? =

Yes, you can! You can make as many forms as you’d like in the free version, without any limitations.

= Which question types are included? =

**Input blocks**
Tripetto naturally supports all the commonly used form question types.

- Checkbox;
- Checkboxes;
- Date (and time);
- Dropdown;
- Email address;
- File upload;
- Matrix;
- Multiple choice;
- Number;
- Paragraph;
- Password;
- Phone number;
- Picture choice;
- Radio buttons;
- Rating;
- Scale;
- Statement;
- Static text;
- Text - Multiple lines;
- Text - Single line;
- URL;
- Yes/No.

**Action blocks**
Enhance your forms with powerful action blocks to perform smart tasks in the background.

- Send email - Send emails from the form, including form data;
- Hidden field - Use hidden fields to enter data in your form;
- Raise error - Prevent a form from submitting.

**Logic**
Use logic to make your forms smart and conversational:

- Branch logic;
- Skip logic;
- Pipe logic;
- Flexible endings.

**Condition blocks**
Make forms smart by intelligently and automatically directing flows with dynamic condition blocks.

- Conditions;
- Evaluate;
- Regex;
- Password match;
- Device Size.

<a href="https://tripetto.com/help/articles/how-to-run-your-form-from-the-wordpress-plugin/" rel="friend" title="More information about building your forms" target="_blank">More information about building your forms over here.</a>

= How do I publish forms in my WP site? =

You can use a shareable link or use the shortcode to embed your forms. <a href="https://tripetto.com/help/articles/how-to-run-your-form-from-the-wordpress-plugin/" rel="friend" title="More information about sharing" target="_blank">More information about sharing over here.</a>

= How do I upgrade to premium? =

There are a few scenarios to upgrade. Generally, you need to take the following steps:

- Purchase the upgrade via our <a href="https://tripetto.com/pricing/wordpress/" rel="friend" title="Upgrade to premium" target="_blank">website</a>, or your WP Admin menu;
- You need a **zip package** with the premium version and your **personal license key**. After purchasing, you can find these in Freemius (our upgrade partner) and/or in your email;
- Upload, install and activate the zip package in your WP Admin. Enter the license key to activate.

== Screenshots ==

1. Example of a customer satisfaction survey in the autoscroll form face
2. Example of a wedding RSVP form in the chat form face
3. Example of a fitness registration form in the autoscroll form face
4. Example of a restaurant reservation form in the classic form face
5. Demo of the customizations in the form builder, including switching form faces
6. Demo of the visual form builder, with easy to use logic

== Translations ==

The following translations are included:

- English (default)
- Dutch (Nederlands)

*Note:* This plugin is designed to be fully localized and translateable, but we need help to translate it to other languages. Take a look at our <a href="https://gitlab.com/tripetto/translations" rel="friend" title="Translations repository" target="_blank">translations repository</a> to see what needs to be done and how you can contribute.

== Changelog ==

{{ CHANGELOG }}
