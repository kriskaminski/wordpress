<?php
namespace Tripetto;

class Export
{
    static function CSV()
    {
        if (!empty($_REQUEST['action']) && $_REQUEST['action'] == "tripetto-export-csv" && Tripetto::assert()) {
            $id = !empty($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
            $stencil = !empty($_REQUEST['stencil']) ? $_REQUEST['stencil'] : '';

            if (!empty($id) && !empty($stencil)) {
                header("content-type: text/csv;charset=utf-8;");
                header("content-disposition: attachment; filename=tripetto-export-$stencil.csv");

                $out = fopen('php://output', 'w');

                global $wpdb;
                global $wp;

                $homeUrl = home_url($wp->request);
                $results = $wpdb->get_results(
                    $wpdb->prepare(
                        "SELECT * FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d AND (stencil=%s OR fingerprint=%s) ORDER BY created DESC",
                        $id,
                        $stencil,
                        $stencil
                    )
                );

                // Add bom (see https://en.wikipedia.org/wiki/Byte_order_mark#UTF-8)
                fputs($out, chr(0xef) . chr(0xbb) . chr(0xbf));

                $header = true;

                foreach ($results as $result) {
                    $fields = json_decode($result->entry, false);

                    if ($header) {
                        $values = array_column($fields->fields, "name");

                        array_unshift($values, __("Date submitted", "tripetto"), __("# Number", "tripetto"), __("Reference", "tripetto"));

                        fputcsv($out, $values, ";");

                        $header = false;
                    }

                    $values = [];

                    array_push($values, mysql2date(get_option('date_format') . ' ' . get_option('time_format'), $result->created));
                    array_push($values, Results::index($result->indx, $result->id, $result->form_id, $result->created));
                    array_push($values, $result->reference);

                    foreach ($fields->fields as $field) {
                        array_push($values, Runner::parseFieldValue($field, true));
                    }

                    fputcsv($out, $values, ";");
                }

                fclose($out);

                return die();
            }

            http_response_code(404);

            die();
        }
    }

    static function register($plugin)
    {
        add_action('init', ['Tripetto\Export', 'CSV']);
    }
}
