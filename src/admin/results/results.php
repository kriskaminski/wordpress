<?php
namespace Tripetto;

require_once __DIR__ . '/list.php';
require_once __DIR__ . '/export.php';

class Results
{
    static function activate($network_wide)
    {
        if (!current_user_can('activate_plugins')) {
            return;
        }

        if (is_multisite() && $network_wide) {
            return;
        }

        Results::database();
    }

    static function database()
    {
        Database::assert(
            "tripetto_entries",
            [
                "form_id int(10) unsigned NOT NULL",
                "indx int(10) unsigned NOT NULL DEFAULT 0",
                "reference varchar(65) NOT NULL DEFAULT ''",
                "entry longtext NOT NULL",
                "fingerprint varchar(65) NOT NULL",
                "stencil varchar(65) NOT NULL DEFAULT ''",
                "signature varchar(65) NOT NULL DEFAULT ''",
                "lang varchar(6) NOT NULL DEFAULT ''",
                "locale varchar(6) NOT NULL DEFAULT ''",
                "created datetime NULL DEFAULT NULL",
            ],
            ["form_id", "indx", "reference", "fingerprint", "stencil", "signature", "created"]
        );
    }

    static function show($id)
    {
        Tripetto::assert();

        if (!empty($id)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT id,name from {$wpdb->prefix}tripetto_forms where id=%d", intval($id)));

            if (!is_null($form)) {
                wp_enqueue_script('wp-tripetto');

                $results = new ResultsList();
                $results->prepare_items();
                $message = "";

                if (!empty($_REQUEST['result_id'])) {
                    $message =
                        '<div class="updated below-h2" id="message"><p>' .
                        sprintf(
                            /* translators: %d is replaced with the number of results deleted */
                            __('Results deleted: %d', 'tripetto'),
                            is_array($_REQUEST['result_id']) ? count($_REQUEST['result_id']) : 1
                        ) .
                        '</p></div>';
                }

                echo '<div class="wrap">';
                echo '<h1 class="wp-heading-inline" style="margin-bottom: 5px;">';

                if (!empty($form->name)) {
                    echo esc_html($form->name) . ' (' . __('results', 'tripetto') . ')';
                } else {
                    echo __('Results', 'tripetto');
                }

                echo '</h1>';
                echo '<a href="?page=tripetto-forms" class="page-title-action">' . __('Back to forms list', 'tripetto') . '</a>';
                echo '<a href="?page=tripetto-forms&action=builder&id=' .
                    strval($form->id) .
                    '" class="page-title-action">' .
                    __('Edit form', 'tripetto') .
                    '</a>';
                echo $message;

                $result_groups = $wpdb->get_results(
                    "SELECT COALESCE(NULLIF(stencil, ''),fingerprint) as stencil_hash, count(*) as entryCount FROM {$wpdb->prefix}tripetto_entries WHERE form_id=$form->id GROUP BY stencil_hash ORDER BY created DESC"
                );

                echo "<div>";

                if (count($result_groups) == 1) {
                    $result_group = $result_groups[0];

                    echo "<a href='?action=tripetto-export-csv&id=$form->id&stencil=$result_group->stencil_hash' class='button action'>" .
                        __('Export results as CSV', 'tripetto') .
                        "</a>";
                } elseif (count($result_groups) > 1) {
                    echo '<select id="form_version">';

                    $version = count($result_groups);

                    foreach ($result_groups as $result_group) {
                        $resultCount = $result_group->entryCount;
                        $label = $resultCount == 1 ? 'result' : 'results';

                        echo "<option value='?action=tripetto-export-csv&id=$form->id&stencil=$result_group->stencil_hash'>" .
                            __('Version', 'tripetto') .
                            " $version ($resultCount $label)</option>";

                        $version--;
                    }
                    echo "</select>";
                    echo "&nbsp;&nbsp;<a onclick='location.href = document.getElementById(\"form_version\").value;' class='button action'>" .
                        __('Export results as CSV', 'tripetto') .
                        "</a>";
                    echo '&nbsp;&nbsp;<a href="https://tripetto.com/help/articles/troubleshooting-seeing-multiple-download-buttons" target="_blank">' .
                        __('Why do I see multiple CSV versions?', 'tripetto') .
                        '</a>';
                }

                echo "</div>";
                echo '<form id="tripetto_results_table" method="GET">';
                echo '<input type="hidden" name="page" value="' . esc_attr($_REQUEST['page']) . '" />';
                echo '<input type="hidden" name="id" value="' . esc_attr($_REQUEST['id']) . '" />';

                $results->display();

                echo '</form>';
                echo '</div>';

                return;
            }
        }

        wp_die(__("Something went wrong, could not fetch the results.", "tripetto"));
    }

    static function view($id)
    {
        Tripetto::assert();

        if (!empty($id)) {
            global $wpdb;

            $result = $wpdb->get_row(
                $wpdb->prepare(
                    "SELECT id,form_id,entry,created,reference,indx from {$wpdb->prefix}tripetto_entries where id=%d",
                    intval($id)
                )
            );

            if (!is_null($result)) {
                if (!is_null($result->entry)) {
                    wp_enqueue_script('wp-tripetto');

                    $data = json_decode($result->entry, false);

                    echo '<div class="wrap">';
                    echo '<h1 class="wp-heading-inline">' .
                        __('Result', 'tripetto') .
                        ' #' .
                        strval(Results::index($result->indx, $result->id, $result->form_id, $result->created)) .
                        '</h1>';
                    echo '<a href="?page=tripetto-forms&action=results&id=' .
                        $result->form_id .
                        '" class="page-title-action">' .
                        __('Back to results', 'tripetto') .
                        '</a>';
                    echo '<div id="poststuff">';
                    echo '<div id="post-body" class="metabox-holder columns-2">';
                    echo '<div id="post-body-content">';
                    echo '<div class="meta-box-sortables ui-sortable">';

                    $count = 0;

                    foreach ($data->fields as $field) {
                        if (!empty($field->string)) {
                            $count++;
                            $value = Runner::parseFieldValue($field);

                            echo '<div class="postbox">';
                            echo '<h2><span>' . nl2br(esc_html($field->name)) . '</span></h2>';
                            echo '<div class="inside">';

                            if (Attachments::isAttachment($field) && !empty($value)) {
                                echo "<a href='{$value}' target='_blank'>" . esc_html($field->string) . "</a>";
                            } else {
                                echo '<p>' . nl2br(esc_html($value)) . ' </p>';
                            }

                            echo '</div>';
                            echo '</div>';
                        }
                    }

                    if (!$count) {
                        echo "This result didn't contain any exportable data.";
                    }

                    echo '</div>';
                    echo '</div>';
                    echo '<div id="postbox-container-1" class="postbox-container">';
                    echo '<div class="meta-box-sortables">';
                    echo '<div class="postbox">';
                    echo '<h2><span>' . __('Information', 'tripetto') . '</span></h2>';
                    echo '<div class="inside">';
                    echo '<p>';

                    $name = $wpdb->get_var($wpdb->prepare("SELECT name from {$wpdb->prefix}tripetto_forms where id=%d", $result->form_id));

                    if (!empty($name)) {
                        echo '<b>' . __('Form name', 'tripetto') . ':</b> ' . esc_html($name) . '<br />';
                    }

                    echo '<b>' .
                        __('Number', 'tripetto') .
                        ':</b> #' .
                        Results::index($result->indx, $result->id, $result->form_id, $result->created) .
                        '<br />';
                    echo '<b>' .
                        __('Date submitted', 'tripetto') .
                        ':</b> ' .
                        mysql2date(get_option('date_format') . ' ' . get_option('time_format'), $result->created);

                    if (!empty($result->reference)) {
                        echo '<br /><b>' . __('Identifier', 'tripetto') . ':</b> ' . $result->reference;
                    }

                    echo '</p>';
                    echo '<br />';
                    echo '<a href="javascript:;" onclick="WPTripetto.showModal(\'' .
                        __('Confirm delete', 'tripetto') .
                        '\',\'' .
                        __('Are you sure you want to delete the result?', 'tripetto') .
                        '\',\'' .
                        __('Yes, delete it', 'tripetto') .
                        '\',\'' .
                        __('No, keep it', 'tripetto') .
                        '\',\'?page=tripetto-forms&action=results&id=' .
                        $result->form_id .
                        '&result_id=' .
                        $id .
                        '\',450,210);" class="button-primary">' .
                        __('Delete Result', 'tripetto') .
                        '</a>';

                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '<br class="clear">';
                    echo '</div>';
                    echo '</div>';

                    return;
                }
            }
        }

        wp_die(__("Something went wrong, could not fetch the result.", "tripetto"));
    }

    static function delete($ids)
    {
        Tripetto::assert();

        if (!empty($ids)) {
            global $wpdb;

            if (is_array($ids)) {
                $ids = implode(',', $ids);
            }

            $attachments = $wpdb->get_col("SELECT id FROM {$wpdb->prefix}tripetto_attachments WHERE entry_id IN ($ids)");

            foreach ($attachments as $attachment) {
                Attachments::delete($attachment);
            }

            $wpdb->query("DELETE FROM {$wpdb->prefix}tripetto_entries WHERE id IN ($ids)");
        }
    }

    static function index($index, $id, $form, $created)
    {
        if (empty($index)) {
            global $wpdb;

            $index = intval(
                $wpdb->get_var(
                    $wpdb->prepare(
                        "SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d AND created<=%s",
                        $form,
                        $created
                    )
                )
            );

            if ($index < 1) {
                $index = 1;
            }

            $wpdb->update($wpdb->prefix . "tripetto_entries", ['indx' => $index], ['id' => $id]);
        }

        return $index;
    }

    static function register($plugin)
    {
        register_activation_hook($plugin, ['Tripetto\Results', 'activate']);

        Export::register($plugin);
    }
}
?>
