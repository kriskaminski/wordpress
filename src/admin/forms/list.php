<?php
namespace Tripetto;

class FormsList extends ListFactory
{
    /**
     * Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     */
    public function __construct()
    {
        parent::__construct([
            'singular' => __('form', 'tripetto'),
            'plural' => __('forms', 'tripetto'),
            'ajax' => false,
        ]);
    }

    /**
     * This is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    /**
     * Render checkbox column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="id[]" value="%s" />', esc_attr($item['id']));
    }

    public function column_name($item)
    {
        global $wp;
        global $wpdb;

        $actions = [
            'edit' => sprintf(
                '<a href="?page=%s&action=builder&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Edit', 'tripetto')
            ),
            'run' => sprintf(
                '<a href="%s?tripetto=%s" target="_blank">%s</a>',
                home_url($wp->request),
                $item['reference'],
                __('Run', 'tripetto')
            ),
            'results' => sprintf(
                '<a href="?page=%s&action=results&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Results', 'tripetto')
            ),
            /*
            'styles' => sprintf(
                '<a href="?page=%s&action=styles&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Styles', 'tripetto')
            ),
            'l10n' => sprintf(
                '<a href="?page=%s&action=l10n&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Translations', 'tripetto')
            ),
            'automate' => sprintf(
                '<a href="?page=%s&action=automate&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Automate', 'tripetto')
            ),
            */
            'duplicate' => sprintf(
                '<a href="?page=%s&action=duplicate&id=%s&nonce=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                wp_create_nonce("tripetto:duplicate:" . strval($item['id'])),
                __('Duplicate', 'tripetto')
            ),
        ];

        if (
            intval($wpdb->get_var($wpdb->prepare("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d", $item['id']))) ==
            0
        ) {
            $actions["delete"] = sprintf(
                '<a href="javascript:;" onclick="WPTripetto.showModal(\'%s\',\'%s\',\'%s\',\'%s\',\'?page=%s&action=delete&id=%s&nonce=%s\',450,210);">%s</a>',
                __('Confirm delete', 'tripetto'),
                __('All the data and the form itself will be removed. This action cannot be made undone.', 'tripetto'),
                __('Yes, delete it', 'tripetto'),
                __('No, keep it', 'tripetto'),
                esc_attr($_REQUEST['page']),
                $item['id'],
                wp_create_nonce("tripetto:delete:" . strval($item['id'])),
                __('Delete', 'tripetto')
            );
        }

        $isInPremiumMode = License::isInPremiumMode();
        $hasPremiumFeatures = License::hasPremiumFeatures($item['id']);

        return sprintf(
            '<a href="?page=%s&action=builder&id=%s">%s %s %s</a>',
            esc_attr($_REQUEST['page']),
            $item['id'],
            esc_html($item['name'] == "" ? "Unnamed form" : $item['name']),
            !$isInPremiumMode && $hasPremiumFeatures ? '<span class="premium">Premium</span>' : '',
            $this->row_actions($actions)
        );
    }

    public function column_shortcode($item)
    {
        global $wp;

        $actions = [
            'clipboard' => sprintf(
                '<a href="javascript:;" onclick="WPTripetto.copyShortcodeToClipboard(%s,\'%s\');">%s</a>',
                $item['id'],
                !empty($item['shortcode']) ? base64_encode($item['shortcode']) : "",
                __('Copy to clipboard', 'tripetto')
            ),
            'shortcode' => sprintf(
                '<a href="?page=%s&action=share&id=%s">%s</a>',
                esc_attr($_REQUEST['page']),
                $item['id'],
                __('Customize', 'tripetto')
            ),
        ];

        return sprintf(
            "<code>[tripetto id='%d'%s]</code>%s",
            $item['id'],
            !empty($item['shortcode']) ? " ... " : "",
            $this->row_actions($actions)
        );

        return;
    }

    /**
     * Render Creation date column
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    public function column_created($item)
    {
        return mysql2date(get_option('date_format') . ' ' . get_option('time_format'), $item['created']);
    }

    public function column_modified($item)
    {
        return mysql2date(get_option('date_format') . ' ' . get_option('time_format'), $item['modified']);
    }

    public function column_results($item)
    {
        global $wpdb;

        $id = $item['id'];

        $results = $wpdb->get_var($wpdb->prepare("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d", $id));
        return "<a href='?page=tripetto-forms&action=results&id={$id}'>$results</a>";
    }

    /**
     * This method return columns to display in table
     *
     * @return array
     */
    function get_columns()
    {
        return [
            'name' => __('Name', 'tripetto'),
            'results' => __('# Results', 'tripetto'),
            'created' => __('Created', 'tripetto'),
            'modified' => __('Modified', 'tripetto'),
            'shortcode' => __('Shortcode', 'tripetto'),
        ];
    }

    /**
     * This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        return [
            'name' => ['name', false],
            'id' => ['id', true],
            'created' => ['created', true],
            'modified' => ['modified', true],
        ];
    }

    /**
     * This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     *
     * @see $this->prepare_items()
     */
    function process_bulk_action()
    {
        global $wpdb;

        $ids = !empty($_REQUEST['id']) ? $_REQUEST['id'] : [];
        $nonce = !empty($_REQUEST['nonce']) ? $_REQUEST['nonce'] : "";

        if (is_array($ids)) {
            $ids = array_map('intval', $ids);
            $ids = implode(',', $ids);
        } else {
            $ids = intval($ids);
        }

        if ('delete' === $this->current_action() && wp_verify_nonce($nonce, 'tripetto:delete:' . strval($ids))) {
            Forms::delete($ids);
        }

        if ('duplicate' === $this->current_action() && wp_verify_nonce($nonce, 'tripetto:duplicate:' . strval($ids))) {
            if (!empty($ids)) {
                $wpdb->query(
                    "INSERT INTO {$wpdb->prefix}tripetto_forms (name,definition,styles,l10n,runner,fingerprint) SELECT name,definition,styles,l10n,runner,fingerprint FROM {$wpdb->prefix}tripetto_forms WHERE id IN ($ids)"
                );
            }
        }

        $formsWithoutRef = $wpdb->get_col("SELECT id FROM {$wpdb->prefix}tripetto_forms WHERE reference IS NULL OR reference=''");

        foreach ($formsWithoutRef as $formWithoutRef) {
            $wpdb->update(
                $wpdb->prefix . "tripetto_forms",
                [
                    'reference' => hash(
                        "sha256",
                        wp_create_nonce('tripetto:form:' . strval($formWithoutRef)) . ":" . strval($formWithoutRef)
                    ),
                ],
                ['id' => $formWithoutRef]
            );
        }
    }

    /**
     * Prepare table list items.
     */
    public function prepare_items()
    {
        global $wpdb;

        $columns = $this->get_columns();
        $hidden = [];
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = [$columns, $hidden, $sortable];

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total = $wpdb->get_var("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_forms");
        $per_page = $total > 0 ? $total : 1;

        // prepare query params, as usual current page, order by and order direction
        $paged = !empty($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged'] - 1) * $per_page) : 0;
        $orderby =
            !empty($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))
                ? $_REQUEST['orderby']
                : 'id';
        $order = !empty($_REQUEST['order']) && in_array($_REQUEST['order'], ['asc', 'desc']) ? $_REQUEST['order'] : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_forms ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged),
            ARRAY_A
        );

        // [REQUIRED] configure pagination
        $this->set_pagination_args([
            'total_items' => $total, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total / $per_page), // calculate pages count
        ]);
    }
}
?>
