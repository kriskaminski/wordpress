<?php
namespace Tripetto;

require_once __DIR__ . '/list.php';

class Forms
{
    static function activate($network_wide)
    {
        if (!current_user_can('activate_plugins')) {
            return;
        }

        if (is_multisite() && $network_wide) {
            return;
        }

        Forms::database();
    }

    static function database()
    {
        Database::assert(
            "tripetto_forms",
            [
                "indx int(10) unsigned NOT NULL DEFAULT 0",
                "reference varchar(65) NOT NULL DEFAULT ''",
                "name text NOT NULL",
                "definition longtext NOT NULL",
                "fingerprint varchar(65) NOT NULL DEFAULT ''",
                "stencil varchar(65) NOT NULL DEFAULT ''",
                "actionables varchar(65) NOT NULL DEFAULT ''",
                "runner tinytext NOT NULL DEFAULT ''",
                "styles longtext NOT NULL DEFAULT ''",
                "l10n longtext NOT NULL DEFAULT ''",
                "hooks longtext NOT NULL DEFAULT ''",
                "shortcode longtext NOT NULL DEFAULT ''",
                "created datetime NULL DEFAULT NULL",
                "modified datetime NULL DEFAULT NULL",
            ],
            ["reference", "fingerprint", "stencil", "created", "modified"]
        );
    }

    static function menu()
    {
        add_submenu_page('tripetto', __('All Forms', 'tripetto'), __('All Forms', 'tripetto'), 'manage_options', 'tripetto-forms', [
            'Tripetto\Forms',
            'page',
        ]);

        add_submenu_page(
            'tripetto',
            __('Add New', 'tripetto'),
            __('Add New', 'tripetto'),
            'manage_options',
            'tripetto-forms&action=create',
            ['Tripetto\Forms', 'page']
        );
    }

    static function page()
    {
        Tripetto::assert();

        global $wpdb;

        wp_enqueue_style('wp-tripetto');

        $action = !empty($_REQUEST['action']) ? $_REQUEST['action'] : "";
        $id = !empty($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

        switch ($action) {
            case "create":
                Builder::create(!empty($_REQUEST['runner']) ? $_REQUEST['runner'] : "");
                break;
            case "builder":
            case "share":
            case "styles":
            case "l10n":
            case "automate":
                Builder::run($id, $action != "builder" ? $action : "");
                break;
            case "results":
                Results::show($id);
                break;
            case "view":
                Results::view($id);
                break;
            default:
                wp_enqueue_script('wp-tripetto');

                $forms = new FormsList();
                $forms->prepare_items();

                $message = "";

                if ($forms->current_action() === "delete" && !empty($_REQUEST['id'])) {
                    $message = '<div class="updated below-h2" id="message"><p>' . __('Form deleted!', 'tripetto') . '</p></div>';
                }

                if ($forms->current_action() === "duplicate" && !empty($_REQUEST['id'])) {
                    $message = '<div class="updated below-h2" id="message"><p>' . __('Form duplicated!', 'tripetto') . '</p></div>';
                }

                echo '<div class="wrap">';
                echo '<h1 class="wp-heading-inline" style="margin-bottom: 5px;">' . __('Tripetto Forms', 'tripetto') . '</h1>';
                echo '<a href="?page=' .
                    esc_attr($_REQUEST['page']) .
                    '&action=create" class="page-title-action">' .
                    __('Create New Form', 'tripetto') .
                    '</a>';
                echo '<hr class="wp-header-end">';
                echo $message;
                echo '<form id="tripetto_forms_table" method="GET">';
                echo '<input type="hidden" name="page" value="' . esc_attr($_REQUEST['page']) . '"/>';
                echo $forms->display();
                echo '</form>';
                echo '</div>';

                break;
        }
    }

    static function delete($ids)
    {
        Tripetto::assert();

        if (!empty($ids)) {
            global $wpdb;

            Results::delete($wpdb->get_col("SELECT id FROM {$wpdb->prefix}tripetto_entries WHERE form_id IN ($ids)"));

            $attachments = $wpdb->get_col("SELECT id FROM {$wpdb->prefix}tripetto_attachments WHERE form_id IN ($ids)");

            foreach ($attachments as $attachment) {
                Attachments::delete($attachment);
            }

            $wpdb->query("DELETE FROM {$wpdb->prefix}tripetto_announcements WHERE form_id IN ($ids)");
            $wpdb->query("DELETE FROM {$wpdb->prefix}tripetto_snapshots WHERE form_id IN ($ids)");
            $wpdb->query("DELETE FROM {$wpdb->prefix}tripetto_forms WHERE id IN ($ids)");
        }
    }

    static function register($plugin)
    {
        register_activation_hook($plugin, ['Tripetto\Forms', 'activate']);

        Builder::register($plugin);
        Results::register($plugin);
    }
}
?>
