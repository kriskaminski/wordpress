import copyTextToClipboard from "copy-text-to-clipboard";
import { Str } from "tripetto";

interface IShortcode {
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly fullPage?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly css?: string;
}

export function copyShortcodeToClipboard(id: number, props: string): void {
    let shortcodeProps: IShortcode | undefined;
    let shortcode = `[tripetto id='${id}'`;

    try {
        shortcodeProps = JSON.parse(atob(props));
    } catch {
        shortcodeProps = undefined;
    }

    if (shortcodeProps) {
        if (shortcodeProps.pausable) {
            shortcode += " pausable='yes'";
        }

        if (shortcodeProps.persistent) {
            shortcode += " persistent='yes'";
        }

        if (shortcodeProps.fullPage) {
            shortcode += " mode='page'";
        }

        if (shortcodeProps.width) {
            shortcode += " width='" + Str.replace(shortcodeProps.width, "'", "") + "'";
        }

        if (shortcodeProps.height) {
            shortcode += " height='" + Str.replace(shortcodeProps.height, "'", "") + "'";
        }

        if (shortcodeProps.css) {
            shortcode += " css='" + encodeURIComponent(shortcodeProps.css).replace(/[']/g, escape) + "'";
        }
    }

    shortcode += "]";

    copyTextToClipboard(shortcode);
}
