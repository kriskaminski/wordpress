import { assert } from "tripetto";
import "./builder.scss";

export function builder(
    props: {
        readonly id: number;
        readonly version: string;
        readonly shareUrl: string;
        readonly open?: "share" | "styles" | "l10n" | "automate";
        readonly tier?: "standard" | "premium";
    },
    baseUrl: string,
    ajaxUrl: string,
    language: string
): void {
    const builderElement = document.createElement("div");

    builderElement.id = "wp-tripetto-builder";

    const builderFrame = assert(
        document.body.appendChild(builderElement).appendChild(document.createElement("iframe")).contentWindow || undefined
    );

    builderFrame.document.open();
    builderFrame.document.write(
        `<body>
           <script src="${baseUrl}/vendors/tripetto-builder.js?ver=${props.version}"></script>
           <script src="${baseUrl}/vendors/tripetto-runner.js?ver=${props.version}"></script>
           <script src="${baseUrl}/js/wp-tripetto-builder.js?ver=${props.version}"></script>
           <script>
           (function(t){t(t)})(function(t){return typeof WPTripettoBuilder===\"undefined\"?setTimeout(function(){t(t)},1):WPTripettoBuilder.bootstrap(${JSON.stringify(
               { ...props, baseUrl, ajaxUrl, language }
           )})});
           </script>
        </body>`
    );
    builderFrame.document.close();

    window.addEventListener("message", (msg: MessageEvent) => {
        if (msg.data) {
            switch (msg.data.type) {
                case "ready":
                    builderElement.classList.add("ready");
                    break;
                case "fullscreen":
                    builderElement.classList.toggle("fullscreen");
                    break;
                case "results":
                    builderElement.classList.remove("ready");

                    window.open(`${window.location.pathname}?page=tripetto-forms&action=results&id=${props.id}`, "_self");
                    break;
                case "close":
                    builderElement.classList.remove("ready");

                    window.open(`${window.location.pathname}?page=tripetto-forms`, "_self");
                    break;
            }
        }
    });
}
