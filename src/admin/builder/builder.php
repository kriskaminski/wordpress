<?php
namespace Tripetto;

class Builder
{
    static function scripts()
    {
        wp_register_script(
            'vendor-tripetto-builder',
            Helpers::pluginUrl() . '/vendors/tripetto-builder.js',
            [],
            $GLOBALS["TRIPETTO_PLUGIN_VERSION"],
            true
        );
    }

    static function create($runner)
    {
        Tripetto::assert();

        global $wpdb;

        switch ($runner) {
            case "chat":
                $runner = "chat";
                break;
            case "classic":
                $runner = "classic";
                break;
            default:
                $runner = "autoscroll";
                break;
        }

        $created = date("Y-m-d H:i:s");
        $wpdb->insert($wpdb->prefix . "tripetto_forms", [
            'runner' => $runner,
            'name' => __("Unnamed form"),
            'definition' => '{}',
            'indx' => 0,
            'created' => $created,
            'modified' => $created,
        ]);

        $id = intval($wpdb->insert_id);

        if (!empty($id)) {
            $reference = hash("sha256", wp_create_nonce('tripetto:form:' . strval($id)) . ":" . strval($id));

            $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}tripetto_forms SET reference=%s WHERE id=%d", $reference, $id));

            $redirect = sprintf('admin.php?page=%s&action=builder&id=%s', esc_attr($_REQUEST['page']), $id);

            echo '<br />🚀 <strong>' . __('Creating new form...', 'tripetto') . '</strong>';
            echo '<script type="text/javascript">window.location="' . $redirect . '";</script>';

            return;
        }

        /* translators: %s contains the error message */
        wp_die(sprintf(__("Something went wrong, could not create a new form (%s).", "tripetto"), $wpdb->last_error));
    }

    static function run($id, $open = "")
    {
        Tripetto::assert();

        $id = intval($id);

        if (!empty($id)) {
            global $wpdb;
            global $wp;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where id=%d", $id));

            if (!is_null($form)) {
                $reference = $form->reference;

                if (empty($reference)) {
                    $reference = hash("sha256", wp_create_nonce('tripetto:form:' . strval($form->id)) . ":" . strval($form->id));

                    $wpdb->update(
                        $wpdb->prefix . "tripetto_forms",
                        [
                            'reference' => $reference,
                        ],
                        ['id' => $form->id]
                    );
                }

                $props = new \stdClass();

                $props->id = $id;
                $props->runner = "autoscroll";
                $props->version = $GLOBALS["TRIPETTO_PLUGIN_VERSION"];
                $props->shareUrl = home_url($wp->request) . "?tripetto=" . $reference;

                if (!empty($open)) {
                    $props->open = $open;
                }

                if (!empty($form->runner)) {
                    $props->runner = $form->runner;
                } elseif (!empty($form->collector)) {
                    switch ($form->collector) {
                        case 'standard-bootstrap':
                            $props->runner = 'classic';
                            break;
                        default:
                            $props->runner = 'autoscroll';
                            break;
                    }
                }

                switch ($props->runner) {
                    case 'chat':
                    case 'classic':
                        break;
                    default:
                        $props->runner = 'autoscroll';
                        break;
                }

                if (Helpers::isValidJSON($form->definition)) {
                    $props->definition = Migration::definition($form, json_decode($form->definition, false));
                }

                $props->styles = Helpers::isValidJSON($form->styles)
                    ? json_decode($form->styles, false)
                    : Migration::styles($form, $props->runner);
                $props->l10n = Helpers::isValidJSON($form->l10n) ? json_decode($form->l10n, false) : Migration::l10n($form);
                $props->hooks = Helpers::isValidJSON($form->hooks) ? json_decode($form->hooks, false) : Migration::hooks($form);

                if (Helpers::isValidJSON($form->shortcode)) {
                    $props->shortcode = json_decode($form->shortcode, false);
                }

                if (License::hasPremiumFeatures($id)) {
                    $props->tier = "premium";
                }

                echo '<br />⌛ <strong>' . __('One moment please...', 'tripetto') . '</strong>';

                $data = str_replace('"_empty_":', '"":', json_encode($props));
                $base = Helpers::pluginUrl();
                $language = get_locale();

                wp_enqueue_script('vendor-tripetto-builder');
                wp_enqueue_script('wp-tripetto');

                wp_add_inline_script('wp-tripetto', "WPTripetto.builder($data, \"$base\", ajaxurl, \"$language\");");

                return;
            }
        }

        wp_die(__("Something went wrong, could not fetch the form.", "tripetto"));
    }

    static function updateDefinition()
    {
        Tripetto::assert();

        $definition = !empty($_POST['definition']) ? wp_unslash($_POST['definition']) : "";
        $name = !empty($_POST['name']) ? wp_unslash($_POST['name']) : "";
        $fingerprint = !empty($_POST['fingerprint']) ? wp_unslash($_POST['fingerprint']) : "";
        $stencil = !empty($_POST['stencil']) ? wp_unslash($_POST['stencil']) : "";
        $actionables = !empty($_POST['actionables']) ? wp_unslash($_POST['actionables']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (
            !empty($id) &&
            Helpers::isValidJSON($definition) &&
            Helpers::isValidSHA256($fingerprint) &&
            Helpers::isValidSHA256($stencil) &&
            Helpers::isValidSHA256($actionables) &&
            strlen($definition) < 4294967295
        ) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE {$wpdb->prefix}tripetto_forms SET name=%s,definition=%s,fingerprint=%s,stencil=%s,actionables=%s,modified=%s WHERE id=%d",
                    substr($name, 0, 65534),
                    $definition,
                    $fingerprint,
                    $stencil,
                    $actionables,
                    date("Y-m-d H:i:s"),
                    $id
                )
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function updateRunner()
    {
        Tripetto::assert();

        $runner = !empty($_POST['runner']) ? wp_unslash($_POST['runner']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (!empty($id) && ($runner === 'autoscroll' || $runner === 'chat' || $runner === 'classic')) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE {$wpdb->prefix}tripetto_forms SET runner=%s,modified=%s WHERE id=%d",
                    $runner,
                    date("Y-m-d H:i:s"),
                    $id
                )
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function updateStyles()
    {
        Tripetto::assert();

        $styles = !empty($_POST['styles']) ? wp_unslash($_POST['styles']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (!empty($id) && Helpers::isValidJSON($styles)) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE {$wpdb->prefix}tripetto_forms SET styles=%s,modified=%s WHERE id=%d",
                    $styles,
                    date("Y-m-d H:i:s"),
                    $id
                )
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function updateL10n()
    {
        Tripetto::assert();

        $l10n = !empty($_POST['l10n']) ? wp_unslash($_POST['l10n']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (!empty($id) && Helpers::isValidJSON($l10n)) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare("UPDATE {$wpdb->prefix}tripetto_forms SET l10n=%s,modified=%s WHERE id=%d", $l10n, date("Y-m-d H:i:s"), $id)
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function updateHooks()
    {
        Tripetto::assert();

        $hooks = !empty($_POST['hooks']) ? wp_unslash($_POST['hooks']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (!empty($id) && Helpers::isValidJSON($hooks)) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE {$wpdb->prefix}tripetto_forms SET hooks=%s,modified=%s WHERE id=%d",
                    $hooks,
                    date("Y-m-d H:i:s"),
                    $id
                )
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function updateShortcode()
    {
        Tripetto::assert();

        $shortcode = !empty($_POST['shortcode']) ? wp_unslash($_POST['shortcode']) : "";
        $id = !empty($_POST['id']) ? intval($_POST['id']) : 0;

        if (!empty($id) && Helpers::isValidJSON($shortcode)) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare(
                    "UPDATE {$wpdb->prefix}tripetto_forms SET shortcode=%s,modified=%s WHERE id=%d",
                    $shortcode,
                    date("Y-m-d H:i:s"),
                    $id
                )
            );

            http_response_code(!empty($wpdb->last_error) ? 500 : 200);
        } else {
            http_response_code(500);
        }

        die();
    }

    static function testSlack()
    {
        Tripetto::assert();

        $url = !empty($_POST['url']) ? wp_unslash($_POST['url']) : "";
        $includeFields = !empty($_POST['includeFields']) && $_POST['includeFields'] == "true" ? true : false;
        if (!empty($url) && filter_var($url, FILTER_VALIDATE_URL)) {
            $message = new \stdClass();

            $message->text = __('This is a test message from Tripetto. Your Slack configuration works ✔', 'tripetto');
            $message->username = "Tripetto";
            $message->icon_url = Helpers::pluginUrl() . "/assets/tripetto.png";

            if ($includeFields) {
                $message->attachments = [
                    [
                        "fields" => [
                            [
                                "title" => __('Test', 'tripetto'),
                                "value" => __('Hello World!', 'tripetto'),
                                "short" => false,
                            ],
                        ],
                    ],
                ];
            }

            $response = wp_safe_remote_post($url, [
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                ],
                'body' => json_encode($message),
                'method' => 'POST',
                'data_format' => 'body',
                'timeout' => 90,
                'redirection' => 5,
                'blocking' => true,
                'httpversion' => '1.0',
            ]);

            if (!is_wp_error($response)) {
                http_response_code(wp_remote_retrieve_response_code($response));

                return die();
            }
        }

        http_response_code(400);

        die();
    }

    static function testWebhook()
    {
        Tripetto::assert();

        $url = !empty($_POST['url']) ? wp_unslash($_POST['url']) : "";
        $nvp = !empty($_POST['nvp']) && $_POST['nvp'] == "true" ? true : false;

        if (!empty($url) && filter_var($url, FILTER_VALIDATE_URL)) {
            $data = new \stdClass();

            if (!empty($nvp)) {
                $data->test = __('Hello World!', 'tripetto');
            } else {
                $data->fingerprint = "d260fcb7a07abf68233107f11a4791956a939e41854e4db31c1930286d83726a";
                $data->fields = [
                    [
                        "name" => __('Test', 'tripetto'),
                        "string" => __('Hello World!', 'tripetto'),
                        "value" => __('Hello World!', 'tripetto'),
                        "time" => 1593696198103,
                        "key" => "4271169a754c5d1ea98813267cf80ab675a7b28df5c7509dd39ef9d794d2e7f4",
                    ],
                ];
            }

            $response = wp_safe_remote_post($url, [
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                ],
                'body' => json_encode($data),
                'method' => 'POST',
                'data_format' => 'body',
                'timeout' => 90,
                'redirection' => 5,
                'blocking' => true,
                'httpversion' => '1.0',
            ]);

            if (!is_wp_error($response)) {
                http_response_code(wp_remote_retrieve_response_code($response));

                return die();
            }
        }

        http_response_code(400);

        die();
    }

    static function register($plugin)
    {
        add_action('wp_ajax_tripetto_definition', ['Tripetto\Builder', 'updateDefinition']);
        add_action('wp_ajax_tripetto_runner', ['Tripetto\Builder', 'updateRunner']);
        add_action('wp_ajax_tripetto_styles', ['Tripetto\Builder', 'updateStyles']);
        add_action('wp_ajax_tripetto_l10n', ['Tripetto\Builder', 'updateL10n']);
        add_action('wp_ajax_tripetto_hooks', ['Tripetto\Builder', 'updateHooks']);
        add_action('wp_ajax_tripetto_shortcode', ['Tripetto\Builder', 'updateShortcode']);
        add_action('wp_ajax_tripetto_test_slack', ['Tripetto\Builder', 'testSlack']);
        add_action('wp_ajax_tripetto_test_webhook', ['Tripetto\Builder', 'testWebhook']);
        add_action('admin_enqueue_scripts', ['Tripetto\Builder', 'scripts']);
    }
}
?>
