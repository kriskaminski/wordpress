<?php
namespace Tripetto;

class Dashboard
{
    static function show()
    {
        Tripetto::assert();

        global $wpdb;

        $total = $wpdb->get_var("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_forms");
        $results = $wpdb->get_var("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries");

        wp_enqueue_style('wp-tripetto');

        // prettier-ignore
        echo '
            <div class="wrap tripetto-wrap">
            <h2></h2>
                <div id="welcome-panel" class="welcome-panel">
                    <div class="welcome-panel-content">
                        <img src="' . Helpers::pluginUrl() . '/assets/tripetto.svg" style="height:50px;widht:50px;" />
                        <h2>' . __('Welcome to Tripetto!', 'tripetto') . '</h2>
                        <p class="about-description">' . __('Give life to forms and surveys. We merged the best of WP Forms, Typeform and Landbot into a single full-fledged plugin for conversational experiences.', 'tripetto') . '</p>
                        <div class="welcome-panel-column-container">
                            <div class="welcome-panel-column">
                                <h3>' . __('Get Started', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-pencil-ruler fa-fw"></i></span>' . __('Create your form using the unique visual form builder', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-eye fa-fw"></i></span>' . __('See a realtime preview of your form, on different devices', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-palette fa-fw"></i></span>' . __('Style your form and remove branding', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                </ul>

                            </div>
                            <div class="welcome-panel-column">
                                <h3>' . __('Publish your form', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-share-alt fa-fw"></i></span>' . __('Share your form using a direct link', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fas fa-file-code fa-fw"></i></span>' . __('Or embed on any page using a shortcode', 'tripetto') . '</code></li>
                                    <li><span class="fa-li"><i class="fas fa-book fa-fw"></i></span>'
                                    /* translators: %s is replaced with the word `here` (that links to a help page) */
                                    . sprintf(__('Learn more about publishing your forms %s', 'tripetto'), '<a href="https://tripetto.com/help/articles/how-to-run-your-form-from-the-wordpress-plugin/" target="_blank">' . __('here', 'tripetto') . '</a>') . '</li>
                                </ul>
                            </div>
                            <div class="welcome-panel-column">
                                <h3>' . __('Collect results and get notified', 'tripetto') . '</h3>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fas fa-file-csv fa-fw"></i></span>' . __('View results and export to CSV', 'tripetto') . '</li>
                                    <li><span class="fa-li"><i class="fab fa-slack fa-fw"></i></span>' . __('Push results to Slack', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                    <li><span class="fa-li"><i class="fas fa-link fa-fw"></i></span>' . __('Push results to webhooks (Zapier, Integromat)', 'tripetto') . '<span class="premium">' . __('Premium', 'tripetto') . '</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="welcome-panel-column-container welcome-panel-footer">
                            ' . ($total > 0 ? '
                                <a class="button button-primary button-hero load-customize" href="?page=tripetto-forms">' . __('View All Forms', 'tripetto') . '</a>
                                <a class="button button-secondary button-hero load-customize" href="?page=tripetto-forms&action=create">' . __('Create New Form', 'tripetto') . '</a>
                            ' : '<a class="button button-primary button-hero load-customize" href="?page=tripetto-forms&action=create">' . __('Start creating your first Tripetto form!', 'tripetto') . '</a>') . '
                            <p>' .
                            /* translators: %1$s is replaced with the word `tutorials`, %2$d is replaced with `examples` and %3$s with `help center` */
                            sprintf(__('Or browse our %1$s and %2$s or visit the %3$s if you need help.', 'tripetto'), '<a href="https://tripetto.com/tutorials/" target="_blank">' . __('tutorials', 'tripetto') . '</a>', '<a href="https://tripetto.com/examples/" target="_blank">' . __('examples', 'tripetto') . '</a>', '<a href="https://tripetto.com/help/wordpress/" target="_blank">' . __('help center', 'tripetto') . '</a>'). '</p>
                        </div>
                    </div>
                </div>
                <div id="dashboard-widgets-wrap">
                    <div id="dashboard-widgets" class="metabox-holder">
                        <div id="postbox-container-1" class="postbox-container">
                            <div id="normal-sortables" class="meta-box-sortables">
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fas fa-chart-line fa-fw"></i>' . __('Statistics', 'tripetto') . '</h2>
                                    <div class="inside" style="height:90px;">
                                        <div class="main">
                                            <ul class="sides">
                                                <li class="statistics-count">
                                                    <p>' . __('# Forms', 'tripetto') . '</p>
                                                    <span>' . $total . '</span>
                                                </li>
                                                <li class="statistics-count">
                                                    <p>' . __('# Results', 'tripetto') . '</p>
                                                    <span>' . $results . '</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="postbox">
                                    <h2 class="hndle"><i class="fas fa-life-ring fa-fw"></i>' . __('Support', 'tripetto') . '</h2>
                                    <div class="inside" style="height:137px;">
                                        <div class="main">
                                            <ul class="sides">
                                                <li>
                                                    <h3>' . __('Need help?', 'tripetto') . '</h3>
                                                    <a href="https://tripetto.com/help/wordpress/" target="_blank">
                                                        <i class="fas fa-book fa-fw"></i>' . __('Visit our Help Center', 'tripetto') . '
                                                    </a>
                                                </li>
                                                <li>
                                                    <h3>' . __("Changelog", 'tripetto') . '</h3>
                                                    <a href="https://gitlab.com/tripetto/wordpress/-/blob/master/CHANGELOG" target="_blank">
                                                        <i class="fas fa-newspaper fa-fw"></i>' . __("What's new in v", 'tripetto') . $GLOBALS["TRIPETTO_PLUGIN_VERSION"] . '
                                                    </a>
                                                </li>
                                            </ul>
                                            <ul class="sides">
                                                <li>
                                                    <h3>' . __('Got a question?', 'tripetto') . '</h3>
                                                    <a href="https://tripetto.com/contact/" target="_blank">
                                                        <i class="fas fa-question-circle fa-fw"></i>' . __('Ask for support', 'tripetto') . '
                                                    </a>
                                                </li>
                                                <li>
                                                    <h3>' . __('Found a bug?', 'tripetto') . '</h3>
                                                    <a href="https://gitlab.com/tripetto/wordpress/issues" target="_blank">
                                                        <i class="fas fa-bug fa-fw"></i>' . __('Submit an issue', 'tripetto') . '
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="postbox-container-2" class="postbox-container">
                            <div class="meta-box-sortables">
                                <div class="postbox" style="height:392px;">
                                    <h2 class="hndle"><i class="fas fa-star fa-fw"></i>' . __('Premium features', 'tripetto') . '</h2>
                                    <div class="inside">
                                        <div class="main">
                                            <h3 class="premium">' . __('Upgrade to premium and unlock the following extras for all your forms:', 'tripetto') . '</h3>
                                            <ul class="fa-ul premium">
                                                <li><span class="fa-li"><i class="fas fa-palette fa-fw"></i></span>' . __('Remove Tripetto branding', 'tripetto') . '</li>
                                                <li><span class="fa-li"><i class="fab fa-slack fa-fw"></i></span>' . __('Receive responses in Slack', 'tripetto') . '</li>
                                                <li><span class="fa-li"><i class="fas fa-link fa-fw"></i></span>' . __('Connect with webhooks (Zapier, Integromat, etc.)', 'tripetto') . '</li>
                                            </ul>
                                            ' . (tripetto_freemius()->is_not_paying() ? ('<a class="button button-primary button-hero load-customize" href="' . tripetto_freemius()->get_upgrade_url() . '">' . __('Upgrade To Premium', 'tripetto') . '</a>') : '<p class="upgraded"><i class="fas fa-check-circle"></i><strong>' . __('Thanks for upgrading!', 'tripetto') . '</strong></p>') . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="postbox-container-3" class="postbox-container">
                            <div class="meta-box-sortables">
                                <div class="postbox" style="height:392px;">
                                    <h2 class="hndle"><i class="fab fa-twitter fa-fw"></i>' . __('Stay tuned - Twitter', 'tripetto') . '</h2>
                                    <a class="twitter-timeline" data-width="100%" data-height="343" data-theme="light" data-chrome="noheader,nofooter,noborders" href="https://twitter.com/tripetto?ref_src=twsrc%5Etfw">Tweets by tripetto</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';
    }
}
?>
