<?php
/**
 * Plugin Name: {{ NAME }}
 * Plugin URI: {{ HOMEPAGE }}
 * Description: {{ DESCRIPTION }}
 * Author: Tripetto
 * Author URI: https://tripetto.com
 * Text Domain: tripetto
 * Domain path: /languages
 * Version: {{ VERSION }}
 *
 * @package Tripetto
 */

namespace Tripetto;

if (!defined('WPINC')) {
    die();
}

if (!defined('ABSPATH')) {
    die();
}

if (function_exists('Tripetto\tripetto_freemius')) {
    tripetto_freemius()->set_basename(true, __FILE__);
} else {
    // Create a helper function for easy SDK access.
    function tripetto_freemius()
    {
        global $tripetto_freemius;

        if (!isset($tripetto_freemius)) {
            // Activate multisite network integration.
            if (!defined('WP_FS__PRODUCT_3825_MULTISITE')) {
                define('WP_FS__PRODUCT_3825_MULTISITE', true);
            }

            // Include Freemius SDK.
            require_once dirname(__FILE__) . '/freemius/start.php';

            $tripetto_freemius = fs_dynamic_init([
                'id' => '3825',
                'slug' => 'tripetto',
                'type' => 'plugin',
                'public_key' => 'pk_bbe3e39e20ddf86c6ff4721c5e30e',
                'secret_key' => '',
                'is_premium' => true,
                'premium_suffix' => 'Premium',
                'has_premium_version' => true,
                'has_addons' => false,
                'has_paid_plans' => true,
                'menu' => [
                    'slug' => 'tripetto',
                    'contact' => false,
                    'support' => false,
                ],
            ]);
        }

        return $tripetto_freemius;
    }

    // Init Freemius.
    tripetto_freemius();
    // Signal that SDK was initiated.
    do_action('tripetto_freemius_loaded');

    $GLOBALS["TRIPETTO_PLUGIN_VERSION"] = "{{ VERSION }}";

    // Libraries and helpers
    require_once __DIR__ . '/lib/attachments.php';
    require_once __DIR__ . '/lib/database.php';
    require_once __DIR__ . '/lib/helpers.php';
    require_once __DIR__ . '/lib/installation.php';
    require_once __DIR__ . '/lib/license.php';
    require_once __DIR__ . '/lib/list.php';
    require_once __DIR__ . '/lib/locale.php';
    require_once __DIR__ . '/lib/mailer.php';
    require_once __DIR__ . '/lib/migration.php';
    require_once __DIR__ . '/lib/template.php';
    require_once __DIR__ . '/lib/translation.php';

    // Views
    require_once __DIR__ . '/admin/admin.php';
    require_once __DIR__ . '/runner/runner.php';

    // Register components
    Installation::register(__FILE__);
    License::register(__FILE__);
    Locale::register(__FILE__);
    Translation::register(__FILE__);
    Attachments::register(__FILE__);
    Tripetto::register(__FILE__);
    Runner::register(__FILE__);
}
?>
