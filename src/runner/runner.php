<?php
namespace Tripetto;

class Runner
{
    static function activate($network_wide)
    {
        if (!current_user_can('activate_plugins')) {
            return;
        }

        if (is_multisite() && $network_wide) {
            return;
        }

        Runner::database();
    }

    static function database()
    {
        Database::assert(
            "tripetto_announcements",
            [
                "form_id int(10) unsigned NOT NULL",
                "nonce varchar(65) NOT NULL DEFAULT ''",
                "fingerprint varchar(65) NOT NULL",
                "checksum varchar(65) NOT NULL",
                "difficulty int(10) unsigned NOT NULL",
                "signature varchar(65) NOT NULL DEFAULT ''",
                "created bigint(20) NOT NULL DEFAULT 0",
            ],
            ["form_id", "nonce", "signature", "created"]
        );

        Database::assert(
            "tripetto_snapshots",
            [
                "reference varchar(65) NOT NULL DEFAULT ''",
                "form_id int(10) unsigned NOT NULL",
                "snapshot longtext NOT NULL",
                "url text NOT NULL",
                "email text NOT NULL",
                "created datetime NULL DEFAULT NULL",
            ],
            ["reference", "form_id", "created"]
        );
    }

    static function getIp()
    {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        foreach (
            [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ]
            as $key
        ) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);

                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }

        return "";
    }

    static function getSignature($fingerprint)
    {
        return hash("sha256", $fingerprint . ":" . Runner::getIp());
    }

    static function runnerNonce($id)
    {
        return 'tripetto:runner:' . $id;
    }

    static function props(
        $form,
        $async = false,
        $fullPage = true,
        $pausable = true,
        $persistent = false,
        $css = '',
        $width = '',
        $height = ''
    ) {
        global $wpdb;
        static $count = 0;

        $count++;

        if (empty($form->reference)) {
            $form->reference = hash("sha256", wp_create_nonce('tripetto:form:' . strval($form->id)) . ":" . strval($form->id));

            $wpdb->update(
                $wpdb->prefix . "tripetto_forms",
                [
                    'reference' => $form->reference,
                ],
                ['id' => $form->id]
            );
        }

        $namespace = "TripettoAutoscroll";
        $props = new \stdClass();
        $props->reference = $form->reference;
        $props->runner = 'autoscroll';
        $props->element = 'tripetto-runner-' . hash('crc32', 'tripetto-runner-' . $count);
        $props->fullPage = $fullPage;
        $props->pausable = $pausable;
        $props->persistent = $persistent;
        $props->css = $css;
        $props->width = $width;
        $props->height = $height;
        $props->language = get_locale();
        $props->url = admin_url('admin-ajax.php');

        if (!empty($form->runner)) {
            $props->runner = $form->runner;
        } elseif (!empty($form->collector)) {
            switch ($form->collector) {
                case 'standard-bootstrap':
                    $props->runner = 'classic';
                    break;
                default:
                    $props->runner = 'autoscroll';
                    break;
            }
        }

        switch ($props->runner) {
            case 'chat':
                $namespace = "TripettoChat";
                break;
            case 'classic':
                $namespace = "TripettoClassic";
                break;
            default:
                $props->runner = 'autoscroll';
                break;
        }

        $props->uri = Helpers::pluginUrl();
        $props->bundles = ["/vendors/tripetto-runner", "/vendors/tripetto-runner-" . $props->runner, "/js/wp-tripetto-runner"];
        $props->namespaces = ["TripettoRunner", $namespace, "WPTripettoRunner"];

        if (!$async) {
            $props->nonce = wp_create_nonce(Runner::runnerNonce($form->id));

            if (!empty($form->definition)) {
                $props->definition = Migration::definition($form, json_decode($form->definition, false));
            }

            if ($pausable) {
                $token = "";

                if (!empty($_REQUEST['tripetto-resume'])) {
                    $token = $_REQUEST['tripetto-resume'];
                } elseif (!empty($_REQUEST['tripetto'])) {
                    $token = $_REQUEST['tripetto'];
                }

                if (!empty($token)) {
                    $snapshot = $wpdb->get_row(
                        $wpdb->prepare("SELECT snapshot,reference from {$wpdb->prefix}tripetto_snapshots where reference=%s", $token)
                    );

                    if (!is_null($snapshot) && !empty($snapshot->snapshot)) {
                        $props->snapshot = json_decode($snapshot->snapshot, false);
                        $props->snapshotToken = $snapshot->reference;
                    }
                }
            }

            $props->styles = Helpers::isValidJSON($form->styles)
                ? json_decode($form->styles, false)
                : Migration::styles($form, $props->runner);

            if (!License::hasPremiumFeatures($form->id)) {
                $props->styles->noBranding = false;
            }

            $props->l10n = Helpers::isValidJSON($form->l10n) ? json_decode($form->l10n, false) : Migration::l10n($form);
        }

        return $props;
    }

    static function script($props)
    {
        $script = '<div id="' . $props->element . '"></div>';
        $script .= '<script>';
        $script .= '(function(t,r,i,p){';
        $script .= 'var a=i.getElementById(t.element),';
        $script .= 'b=t.bundles.map(function(s){return t.uri+s+".js?ver=' . $GLOBALS['TRIPETTO_PLUGIN_VERSION'] . '"}),';
        $script .= 'c=function(d){';
        $script .= 'd.forEach(function(e){';
        $script .= 'if(!r[e]){';
        $script .= 'var b=i.createElement(p);';
        $script .= 'r[e]=true;';
        $script .= 'b.async=true;';
        $script .= 'b.src=e;';
        $script .= 'a.parentNode.insertBefore(b,a)';
        $script .= '}';
        $script .= '})';
        $script .= '};';
        $script .=
            '(function(f){f(f)})(function(f){c([b[0]]);typeof r[t.namespaces[0]]!=="undefined"&&t.namespaces.filter(function(s){c(b);return typeof r[s]==="undefined"}).length===0?WPTripettoRunner.run(t):setTimeout(function(){f(f)},1)});';
        $script .= '})(' . str_replace('"_empty_":', '"":', json_encode($props)) . ',window,document,"script");';
        $script .= '</script>';
        $script .=
            '<noscript>' .
            __(
                'Normally you should see a Tripetto form over here, but it needs JavaScript to run properly and it seems that is disabled in your browser. Please enable JavaScript to see and use the form.',
                'tripetto'
            ) .
            '</noscript>';

        return $script;
    }

    static function shortcode($atts)
    {
        extract(
            $atts = shortcode_atts(
                [
                    'id' => '0',
                    'mode' => 'inline',
                    'pausable' => 'no',
                    'persistent' => 'no',
                    'css' => '',
                    'width' => '',
                    'height' => '',
                ],
                $atts,
                'tripetto'
            )
        );

        $id = !empty($atts['id']) ? intval($atts['id']) : 0;
        $fullPage = !empty($atts['mode']) && ($atts['mode'] === 'page' || $atts['mode'] === 'overlay') ? true : false;
        $pausable = !empty($atts['pausable']) && ($atts['pausable'] === 'yes' || $atts['pausable'] === 'true') ? true : false;
        $persistent = !empty($atts['persistent']) && ($atts['persistent'] === 'yes' || $atts['persistent'] === 'true') ? true : false;
        $css = !empty($atts['css']) ? urldecode($atts['css']) : '';
        $width = !empty($atts['width']) ? $atts['width'] : '';
        $height = !empty($atts['height']) ? $atts['height'] : '';

        if (!empty($id)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where id=%d", $id));

            if (!is_null($form)) {
                return Runner::script(Runner::props($form, true, $fullPage, $pausable, $persistent, $css, $width, $height));
            }
        }

        return is_user_logged_in() ? 'Invalid shortcode!' : '';
    }

    static function isDate($field)
    {
        return $field->type == "tripetto-block-date";
    }

    static function parseFieldValue($field)
    {
        if (empty($field->string)) {
            return "";
        }

        if (Runner::isDate($field)) {
            if (strpos($field->string, ' ') !== false) {
                return date_i18n(get_option('date_format') . ' ' . get_option('time_format'), intval($field->value) / 1000);
            }

            return date_i18n(get_option('date_format'), intval($field->value) / 1000);
        }

        if (Attachments::isAttachment($field)) {
            global $wp;

            if (empty($field->reference)) {
                return "";
            }

            return home_url($wp->request) . '/wp-admin/admin.php?action=tripetto-attachment&reference=' . urlencode($field->reference);
        }

        return $field->string;
    }

    static function parseFieldsToContent($dataset)
    {
        $content = '';

        foreach ($dataset->fields as $field) {
            if (!empty($field->name)) {
                $value = Runner::parseFieldValue($field);

                if (!empty($value)) {
                    if (Attachments::isAttachment($field)) {
                        $content .= sprintf(
                            '<p><b>%s</b><br><a href="%s" target="_blank">%s</a></p>',
                            nl2br(esc_html($field->name)),
                            esc_html($value),
                            esc_html($field->string)
                        );
                    } else {
                        $content .= sprintf('<p><b>%s</b><br>%s</p>', nl2br(esc_html($field->name)), nl2br(esc_html($value)));
                    }
                }
            }
        }

        return $content;
    }

    static function exportablesStencil($exportables)
    {
        $hash = hash("sha256", "stencil:exportables");

        foreach ($exportables->fields as $field) {
            $hash = hash("sha256", $hash . $field->node->id . $field->datatype . $field->slot . $field->node->context);
        }

        return $hash == $exportables->stencil ? $hash : hash("sha256", $hash . $exportables->stencil);
    }

    static function actionablesStencil($actionables)
    {
        $hash = hash("sha256", "stencil:actionables");

        if (!empty($actionables)) {
            foreach ($actionables->nodes as $node) {
                foreach ($node->data as $data) {
                    $hash = hash("sha256", $hash . $node->node->id . $data->datatype . $data->slot . $node->node->context);
                }
            }

            if ($actionables->stencil != $hash) {
                return hash("sha256", $hash . $actionables->stencil);
            }
        }

        return $hash;
    }

    static function checksum($exportables, $actionables)
    {
        $hash = hash("sha256", $exportables->fingerprint . Runner::exportablesStencil($exportables));

        foreach ($exportables->fields as $field) {
            $hash = hash(
                "sha256",
                $hash .
                    $field->key .
                    (isset($field->time) ? strval($field->time) : "") .
                    substr($field->string, 0, 4096) .
                    (isset($field->reference) && is_string($field->reference) ? substr($field->reference, 0, 4096) : "")
            );
        }

        if (!empty($actionables)) {
            $hash = hash("sha256", $hash . $actionables->fingerprint . Runner::actionablesStencil($actionables));

            foreach ($actionables->nodes as $node) {
                $hash = hash("sha256", $hash . $node->key);

                foreach ($node->data as $data) {
                    $hash = hash(
                        "sha256",
                        $hash .
                            $data->key .
                            (isset($data->time) ? strval($data->time) : "") .
                            substr($data->string, 0, 4096) .
                            (isset($data->reference) && is_string($data->reference) ? substr($data->reference, 0, 4096) : "")
                    );
                }
            }
        }

        return $hash;
    }

    static function dataset($exportables, $id, $index)
    {
        $dataset = new \stdClass();

        $dataset->id = $id;
        $dataset->index = $index;
        $dataset->created = str_replace('+00:00', '.000Z', gmdate(DATE_ATOM));
        $dataset->fingerprint = $exportables->fingerprint;
        $dataset->fields = $exportables->fields;

        return $dataset;
    }

    static function powUInt32LeftShift($a, $b)
    {
        if (PHP_INT_SIZE >= 8) {
            return ($a << $b) & 0xffffffff;
        }

        return $a << $b;
    }

    static function powReadUInt32($buffer, $offset)
    {
        $result =
            Runner::powUInt32LeftShift($buffer[$offset], 24) |
            Runner::powUInt32LeftShift($buffer[$offset + 1], 16) |
            Runner::powUInt32LeftShift($buffer[$offset + 2], 8) |
            $buffer[$offset + 3];

        if (PHP_INT_SIZE >= 8) {
            return $result & 0xffffffff;
        }

        return $result;
    }

    static function powVerifyDifficulty($hash, $difficulty)
    {
        $buffer = [];
        $size = strlen($hash) / 2;
        $offset = 0;
        $i = 0;

        if ($difficulty >= $size * 8) {
            return false;
        }

        for (; $i < $size; $i++) {
            array_push($buffer, intval($hash[$i * 2] . $hash[$i * 2 + 1], 16));
        }

        for ($i = 0; $i <= $difficulty - 8; $i += 8, $offset++) {
            if ($offset >= $size || $buffer[$offset] !== 0) {
                return false;
            }
        }

        return $offset < $size && ($buffer[$offset] & Runner::powUInt32LeftShift(0xff, 8 + $i - $difficulty)) == 0;
    }

    static function powVerify($nonce, $difficulty, $validity, $checksum, $id)
    {
        if (strlen($nonce) < 16 || strlen($nonce) > 64 || strlen($nonce) % 16) {
            return false;
        }

        $buffer = [];
        $size = strlen($nonce) / 2;

        for ($i = 0; $i < $size; $i++) {
            array_push($buffer, intval($nonce[$i * 2] . $nonce[$i * 2 + 1], 16));
        }

        $age = (time() + 1) * 1000 - (Runner::powReadUInt32($buffer, 0) * 0x100000000 + Runner::powReadUInt32($buffer, 4));

        if ($validity > 0 && ($age < 0 || $age > $validity)) {
            return false;
        }

        if (Runner::powVerifyDifficulty(hash("sha256", $checksum . $id . $nonce), $difficulty)) {
            return true;
        }

        return false;
    }

    static function definition()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";

        if (Helpers::isValidSHA256($reference)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            if (!is_null($form) && Helpers::isValidJSON($form->definition)) {
                $response = new \stdClass();

                $response->nonce = wp_create_nonce(Runner::runnerNonce($form->id));
                $response->definition = Migration::definition($form, json_decode($form->definition, false));

                header('Content-Type: application/json');

                echo str_replace('"_empty_":', '"":', json_encode($response));
                http_response_code(200);

                return die();
            }
        }

        http_response_code(400);

        die();
    }

    static function styles()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";

        if (Helpers::isValidSHA256($reference)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            if (!is_null($form)) {
                $runner = "autoscroll";

                if (!empty($form->runner)) {
                    $runner = $form->runner;
                } elseif (!empty($form->collector)) {
                    switch ($form->collector) {
                        case 'standard-bootstrap':
                            $runner = 'classic';
                            break;
                        default:
                            $runner = 'autoscroll';
                            break;
                    }
                }

                $styles = Helpers::isValidJSON($form->styles) ? json_decode($form->styles, false) : Migration::styles($form, $runner);

                if (!License::hasPremiumFeatures($form->id)) {
                    $styles->noBranding = false;
                }

                header('Content-Type: application/json');

                echo str_replace('"_empty_":', '"":', json_encode($styles));

                http_response_code(200);

                return die();
            }
        }

        http_response_code(400);

        die();
    }

    static function l10n()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";

        if (Helpers::isValidSHA256($reference)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            if (!is_null($form)) {
                header('Content-Type: application/json');

                echo str_replace(
                    '"_empty_":',
                    '"":',
                    json_encode(Helpers::isValidJSON($form->l10n) ? json_decode($form->l10n, false) : Migration::l10n($form))
                );

                http_response_code(200);

                return die();
            }
        }

        http_response_code(400);

        die();
    }

    static function snapshot()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $token = !empty($_POST['token']) ? $_POST['token'] : "";

        if (Helpers::isValidSHA256($reference) && Helpers::isValidSHA256($token)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT id from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            if (!is_null($form)) {
                $snapshot = $wpdb->get_row(
                    $wpdb->prepare(
                        "SELECT snapshot from {$wpdb->prefix}tripetto_snapshots where form_id=%d AND reference=%s",
                        $form->id,
                        $token
                    )
                );

                if (!is_null($snapshot) && !empty($snapshot->snapshot)) {
                    header('Content-Type: application/json');

                    echo $snapshot->snapshot;

                    http_response_code(200);

                    return die();
                } else {
                    http_response_code(404);

                    die();
                }
            }
        }

        http_response_code(400);

        die();
    }

    static function announce()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $fingerprint = !empty($_POST['fingerprint']) ? $_POST['fingerprint'] : "";
        $checksum = !empty($_POST['checksum']) ? $_POST['checksum'] : "";

        if (
            Helpers::isValidSHA256($reference) &&
            !empty($nonce) &&
            Helpers::isValidSHA256($fingerprint) &&
            Helpers::isValidSHA256($checksum)
        ) {
            global $wpdb;

            $form = $wpdb->get_row(
                $wpdb->prepare("SELECT id,fingerprint from {$wpdb->prefix}tripetto_forms where reference=%s", $reference)
            );

            if (
                !is_null($form) &&
                wp_verify_nonce($nonce, Runner::runnerNonce($form->id)) &&
                (empty($form->fingerprint) || $form->fingerprint == $fingerprint)
            ) {
                $signature = Runner::getSignature($fingerprint);
                $timestamp = time() * 1000;
                $difficulty =
                    12 +
                    intval(
                        $wpdb->get_var(
                            $wpdb->prepare(
                                "SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d AND signature=%s AND created > (NOW() - INTERVAL 10 MINUTE)",
                                $form->id,
                                $signature
                            )
                        )
                    );

                $wpdb->query(
                    $wpdb->prepare("DELETE FROM {$wpdb->prefix}tripetto_announcements WHERE created<%d", $timestamp - 60 * 60 * 1000)
                );

                $pending = $wpdb->get_results(
                    $wpdb->prepare(
                        "SELECT difficulty FROM {$wpdb->prefix}tripetto_announcements WHERE signature=%s AND created>%d",
                        $signature,
                        $timestamp - 10 * 60 * 1000
                    )
                );

                if (count($pending) > 0) {
                    foreach ($pending as $pendingAnnouncement) {
                        $difficulty = max($difficulty + 2, $pendingAnnouncement->difficulty + 2);
                    }
                }

                $wpdb->insert($wpdb->prefix . "tripetto_announcements", [
                    'form_id' => $form->id,
                    'fingerprint' => $fingerprint,
                    'checksum' => $checksum,
                    'difficulty' => $difficulty,
                    'signature' => $signature,
                    'created' => $timestamp,
                ]);

                if (!empty($wpdb->insert_id)) {
                    $nonce = wp_create_nonce('tripetto:runner:submit:' . $wpdb->insert_id);

                    if (!empty($nonce)) {
                        $wpdb->update($wpdb->prefix . "tripetto_announcements", ['nonce' => $nonce], ['id' => $wpdb->insert_id]);

                        header('Content-Type: application/json');

                        $announcement = new \stdClass();
                        $announcement->id = $nonce;
                        $announcement->difficulty = $difficulty;
                        $announcement->timestamp = $timestamp;

                        http_response_code(200);

                        echo json_encode($announcement);

                        return die();
                    }
                }
            } else {
                http_response_code(409);

                die();
            }
        }

        http_response_code(400);

        die();
    }

    static function submit()
    {
        $announcementId = !empty($_POST['id']) ? $_POST['id'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $language = !empty($_POST['language']) ? $_POST['language'] : "";
        $locale = !empty($_POST['locale']) ? $_POST['locale'] : "";
        $exportables = !empty($_POST['exportables']) ? wp_unslash($_POST['exportables']) : "";
        $actionables = !empty($_POST['actionables']) ? wp_unslash($_POST['actionables']) : "";

        if (
            !empty($announcementId) &&
            !empty($nonce) &&
            Helpers::isValidJSON($exportables) &&
            (empty($actionables) || Helpers::isValidJSON($actionables))
        ) {
            global $wpdb;

            $wpdb->query(
                $wpdb->prepare("DELETE FROM {$wpdb->prefix}tripetto_announcements WHERE created<%d", time() * 1000 - 60 * 60 * 1000)
            );

            $announcement = $wpdb->get_row(
                $wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_announcements WHERE nonce=%s", $announcementId)
            );

            if (!is_null($announcement)) {
                $form = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}tripetto_forms WHERE id={$announcement->form_id}");

                if (!is_null($form)) {
                    $exportables = json_decode($exportables, false);
                    $actionables = !empty($actionables) ? json_decode($actionables, false) : null;
                    $checksum = Runner::checksum($exportables, $actionables);

                    if (
                        Helpers::isValidSHA256($announcement->fingerprint) &&
                        $announcement->fingerprint == $exportables->fingerprint &&
                        Helpers::isValidSHA256($exportables->stencil) &&
                        (empty($form->stencil) || $exportables->stencil == $form->stencil) &&
                        (!$actionables ||
                            ($actionables->fingerprint == $announcement->fingerprint &&
                                Helpers::isValidSHA256($actionables->stencil) &&
                                (empty($form->actionables) || $actionables->stencil == $form->actionables))) &&
                        $checksum == $announcement->checksum &&
                        Runner::powVerify($nonce, $announcement->difficulty, 1000 * 60 * 15, $checksum, $announcementId)
                    ) {
                        $index = intval($form->indx) + 1;

                        if ($index <= 1) {
                            $count = intval(
                                $wpdb->get_var(
                                    $wpdb->prepare("SELECT COUNT(id) FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d", $form->id)
                                )
                            );

                            $last = intval(
                                $wpdb->get_var(
                                    $wpdb->prepare(
                                        "SELECT indx FROM {$wpdb->prefix}tripetto_entries WHERE form_id=%d AND indx>0 ORDER BY indx DESC LIMIT 1",
                                        $form->id
                                    )
                                )
                            );

                            $index = max($count, $last) + 1;
                        }

                        $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}tripetto_announcements WHERE nonce=%s", $announcementId));

                        $dataset = Runner::dataset($exportables, $announcementId, $index);
                        $datastring = json_encode($dataset);

                        if (strlen($datastring) < 4294967295) {
                            if (empty($form->fingerprint)) {
                                $wpdb->update(
                                    $wpdb->prefix . "tripetto_forms",
                                    [
                                        'fingerprint' => $exportables->fingerprint,
                                        'stencil' => Runner::exportablesStencil($exportables),
                                        'actionables' => Runner::actionablesStencil($actionables),
                                    ],
                                    ['id' => $form->id]
                                );
                            }

                            $migrateEntries = $wpdb->get_row(
                                $wpdb->prepare(
                                    "SELECT fingerprint FROM {$wpdb->prefix}tripetto_entries WHERE stencil=%s AND form_id=%d AND created>%d ORDER BY created DESC",
                                    '',
                                    $form->id,
                                    date_create($form->modified)->getTimestamp()
                                )
                            );

                            if (!is_null($migrateEntries) && !empty($migrateEntries->fingerprint)) {
                                $wpdb->update(
                                    $wpdb->prefix . "tripetto_entries",
                                    [
                                        'fingerprint' => $exportables->fingerprint,
                                        'stencil' => $exportables->stencil,
                                    ],
                                    [
                                        'form_id' => $form->id,
                                        'fingerprint' => $migrateEntries->fingerprint,
                                        'stencil' => '',
                                    ]
                                );
                            }

                            $wpdb->insert($wpdb->prefix . "tripetto_entries", [
                                'form_id' => $form->id,
                                'reference' => $announcementId,
                                'indx' => $index,
                                'entry' => $datastring,
                                'fingerprint' => $exportables->fingerprint,
                                'stencil' => $exportables->stencil,
                                'signature' => $announcement->signature,
                                'lang' => substr($language, 0, 5),
                                'locale' => substr($locale, 0, 5),
                                'created' => date("Y-m-d H:i:s"),
                            ]);

                            $id = intval($wpdb->insert_id);

                            if (!empty($id)) {
                                $wpdb->update($wpdb->prefix . "tripetto_forms", ['indx' => $index], ['id' => $form->id]);

                                if (!empty($_POST['snapshot'])) {
                                    $wpdb->query(
                                        $wpdb->prepare(
                                            "DELETE FROM {$wpdb->prefix}tripetto_snapshots WHERE reference=%s",
                                            $_POST['snapshot']
                                        )
                                    );
                                }

                                Attachments::validate($exportables, $id);
                                Runner::actions($form, $actionables, $id, $dataset);
                                Runner::hooks($form, $id, $dataset);

                                do_action('tripetto_submit', $dataset);

                                echo $announcementId;

                                http_response_code(200);

                                return die();
                            }
                        }
                    } else {
                        http_response_code(403);

                        die();
                    }
                }
            }
        }

        http_response_code(400);

        die();
    }

    static function handlebars($content, $dataset)
    {
        $content = str_replace('{{tripetto.index}}', strval($dataset->index), $content);
        $content = str_replace('{{tripetto.id}}', $dataset->id, $content);

        return $content;
    }

    static function actions($form, $actionables, $id, $dataset)
    {
        if (!empty($actionables)) {
            foreach ($actionables->nodes as $node) {
                switch ($node->type) {
                    case "tripetto-block-mailer":
                        $recipientField = array_filter($node->data, function ($data) {
                            return $data->slot == 'recipient';
                        });
                        $recipient = count($recipientField) == 1 ? reset($recipientField)->string : "";

                        $subjectField = array_filter($node->data, function ($data) {
                            return $data->slot == 'subject';
                        });
                        $subject = count($subjectField) == 1 ? Runner::handlebars(reset($subjectField)->string, $dataset) : "";
                        $messageField = array_filter($node->data, function ($data) {
                            return $data->slot == 'message';
                        });
                        $message = count($messageField) == 1 ? Runner::handlebars(reset($messageField)->string, $dataset) : "";

                        $senderField = array_filter($node->data, function ($data) {
                            return $data->slot == 'sender';
                        });
                        $sender = count($senderField) == 1 ? reset($senderField)->string : "";

                        $includeFields = array_filter($node->data, function ($data) {
                            return $data->slot == 'data';
                        });
                        $includeFields = count($includeFields) == 1 && !empty(reset($includeFields)->value);

                        if (!empty($recipient) && !empty($subject)) {
                            Mailer::send(
                                $recipient,
                                $subject,
                                Template::render('mailer.php', [
                                    'message' => nl2br(esc_html($message)),
                                    'fields' => !empty($includeFields) ? Runner::parseFieldsToContent($dataset) : '',
                                    'footer' => License::hasPremiumFeatures($form->id)
                                        ? ""
                                        : Template::render('footer.php', [
                                            'recipient' => esc_html($recipient),
                                            'url' => Helpers::pluginUrl(),
                                        ]),
                                ]),
                                $sender
                            );
                        }
                        break;
                }
            }
        }
    }

    static function hooks($form, $id, $dataset)
    {
        $hooks = Helpers::isValidJSON($form->hooks) ? json_decode($form->hooks, false) : Migration::hooks($form);
        $formName = !empty($form->name) ? $form->name : __('Unnamed form', 'tripetto');

        if (!empty($hooks->email) && !empty($hooks->email->enabled) && !empty($hooks->email->recipient)) {
            $url = Helpers::pluginUrl();

            Mailer::send(
                $hooks->email->recipient,
                /* translators: %1$s is replaced with the form name and %2$d is replaced with the form index */
                sprintf(__('New submission from %1$s (#%2$d)', 'tripetto'), $formName, $dataset->index),
                Template::render('notification.php', [
                    'name' => esc_html($formName),
                    'recipient' => esc_html($hooks->email->recipient),
                    'url' => $url,
                    'index' => strval($dataset->index),
                    'fields' => !empty($hooks->email->includeFields) ? Runner::parseFieldsToContent($dataset) : '',
                    'footer' => License::hasPremiumFeatures($form->id)
                        ? ""
                        : Template::render('footer.php', [
                            'recipient' => esc_html($hooks->email->recipient),
                            'url' => $url,
                        ]),
                    'viewUrl' => sprintf('%s?page=tripetto-forms&action=view&id=%d', admin_url('admin.php'), $id),
                ])
            );
        }

        if (License::hasPremiumFeatures($form->id)) {
            if (
                !empty($hooks->slack) &&
                !empty($hooks->slack->enabled) &&
                !empty($hooks->slack->url) &&
                filter_var($hooks->slack->url, FILTER_VALIDATE_URL)
            ) {
                $message = new \stdClass();

                /* translators: %1$d is replaced with the form index and %2$s is replaced with the form name */
                $message->text = sprintf(__('New submission `#%1$d` for `%2$s`', 'tripetto'), $dataset->index, $formName);
                $message->username = "Tripetto";
                $message->icon_url = Helpers::pluginUrl() . "/assets/tripetto.png";

                if (!empty($hooks->slack->includeFields)) {
                    $attachments = [];

                    foreach ($dataset->fields as $field) {
                        $value = Runner::parseFieldValue($field);

                        if (!empty($value)) {
                            array_push($attachments, [
                                "title" => $field->name,
                                "value" => $value,
                                "short" => false,
                            ]);
                        }
                    }

                    $message->attachments = [
                        [
                            "fields" => $attachments,
                        ],
                    ];
                }

                wp_safe_remote_post($hooks->slack->url, [
                    'headers' => [
                        'Content-Type' => 'application/json; charset=utf-8',
                    ],
                    'body' => json_encode($message),
                    'method' => 'POST',
                    'data_format' => 'body',
                    'timeout' => 90,
                    'redirection' => 5,
                    'blocking' => true,
                    'httpversion' => '1.0',
                ]);
            }
        }

        if (
            !empty($hooks->webhook) &&
            !empty($hooks->webhook->enabled) &&
            !empty($hooks->webhook->url) &&
            filter_var($hooks->webhook->url, FILTER_VALIDATE_URL)
        ) {
            $data = json_encode($dataset);

            if (!empty($hooks->webhook->nvp)) {
                $data = new \stdClass();

                $data->tripettoId = $dataset->id;
                $data->tripettoIndex = $dataset->index;
                $data->tripettoCreateDate = $dataset->created;
                $data->tripettoFingerprint = $dataset->fingerprint;

                foreach ($dataset->fields as $field) {
                    if (!empty($field->name)) {
                        $value = Runner::parseFieldValue($field);

                        if (!empty($value)) {
                            $name = $field->name;
                            $counter = 1;

                            while (!empty($data->{$name})) {
                                $counter++;
                                $name = $field->name . "(" . $counter . ")";
                            }

                            $data->{$name} = $value;
                        }
                    }
                }

                $data = json_encode($data);
            }

            wp_safe_remote_post($hooks->webhook->url, [
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                ],
                'body' => $data,
                'method' => 'POST',
                'data_format' => 'body',
                'timeout' => 90,
                'redirection' => 5,
                'blocking' => true,
                'httpversion' => '1.0',
            ]);
        }
    }

    static function pause()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $url = !empty($_POST['url']) ? $_POST['url'] : "";
        $emailAddress = !empty($_POST['emailAddress']) ? $_POST['emailAddress'] : "";
        $snapshot = !empty($_POST['snapshot']) ? wp_unslash($_POST['snapshot']) : "";

        if (
            Helpers::isValidSHA256($reference) &&
            !empty($nonce) &&
            !empty($emailAddress) &&
            filter_var($emailAddress, FILTER_VALIDATE_EMAIL) &&
            strlen($emailAddress) < 65535 &&
            !empty($url) &&
            filter_var($url, FILTER_VALIDATE_URL) &&
            strlen($url) < 65535 &&
            strlen($snapshot) < 4294967295 &&
            Helpers::isValidJSON($snapshot)
        ) {
            global $wpdb;

            $form = $wpdb->get_row(
                $wpdb->prepare("SELECT id,definition from {$wpdb->prefix}tripetto_forms where reference=%s", $reference)
            );

            if (!is_null($form) && wp_verify_nonce($nonce, Runner::runnerNonce($form->id))) {
                $reference = "";

                if (!empty($_POST['token'])) {
                    $current = $wpdb->get_row(
                        $wpdb->prepare(
                            "SELECT reference,id from {$wpdb->prefix}tripetto_snapshots where reference=%s AND form_id=%d",
                            $_POST['token'],
                            $form->id
                        )
                    );

                    if (!empty($current)) {
                        $reference = $current->reference;

                        $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}tripetto_snapshots WHERE id=%d", $current->id));
                    }
                }

                $wpdb->insert($wpdb->prefix . "tripetto_snapshots", [
                    'form_id' => $form->id,
                    'reference' => $reference,
                    'snapshot' => $snapshot,
                    'url' => '',
                    'email' => $emailAddress,
                    'created' => date("Y-m-d H:i:s"),
                ]);

                $snapshotId = intval($wpdb->insert_id);

                if (!empty($snapshotId)) {
                    if (empty($reference)) {
                        $reference = hash("sha256", wp_create_nonce(Runner::runnerNonce($snapshotId)) . ":" . strval($snapshotId));
                    }

                    $n = strpos($url, 'tripetto=');

                    if ($n !== false) {
                        $url = substr($url, 0, $n + 9) . $reference;
                    } else {
                        $n = strpos($url, 'tripetto-resume=');

                        if ($n !== false) {
                            $url = substr($url, 0, $n + 16) . $reference;
                        } else {
                            $url .= (strpos($url, '?') !== false ? "&" : "?") . "tripetto-resume={$reference}";
                        }
                    }

                    $wpdb->update($wpdb->prefix . "tripetto_snapshots", ['reference' => $reference, 'url' => $url], ['id' => $snapshotId]);

                    Mailer::send(
                        $emailAddress,
                        __('Resume your form with this magic link', 'tripetto'),
                        Template::render('snapshot.php', [
                            'url' => $url,
                            'footer' => License::hasPremiumFeatures($form->id)
                                ? ""
                                : Template::render('footer.php', [
                                    'recipient' => esc_html($emailAddress),
                                    'url' => Helpers::pluginUrl(),
                                ]),
                        ])
                    );

                    $dataset = new \stdClass();

                    $dataset->id = $snapshotId;
                    $dataset->form = $form->id;
                    $dataset->reference = $reference;
                    $dataset->url = $url;
                    $dataset->emailAddress = $emailAddress;

                    do_action('tripetto_pause', $dataset);

                    http_response_code(200);

                    return die();
                }
            }
        }

        http_response_code(400);

        die();
    }

    static function standalone()
    {
        if (isset($_REQUEST['tripetto']) && Helpers::isValidSHA256($_REQUEST['tripetto'])) {
            global $wp;
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where reference=%s", $_REQUEST['tripetto']));

            if (is_null($form)) {
                $snapshot = $wpdb->get_row(
                    $wpdb->prepare("SELECT form_id from {$wpdb->prefix}tripetto_snapshots where reference=%s", $_REQUEST['tripetto'])
                );

                if (!is_null($snapshot)) {
                    $form = $wpdb->get_row($wpdb->prepare("SELECT * from {$wpdb->prefix}tripetto_forms where id=%d", $snapshot->form_id));
                }
            }

            if (!is_null($form)) {
                $props = Runner::props($form);
                $description = '';
                $keywords = '';
                $language = '';
                $url = home_url($wp->request) . '?tripetto=' . $form->reference;

                if (!empty($props->definition)) {
                    if (!empty($props->definition->description)) {
                        $description = strval($props->definition->description);
                    }

                    if (!empty($props->definition->keywords)) {
                        $keywords = implode(",", $props->definition->keywords);
                    }

                    if (!empty($props->definition->language)) {
                        $language = strval($props->definition->language);
                    }
                }

                echo '<!DOCTYPE html>';
                echo '<html' . (!empty($language) ? ' lang="' . esc_attr($language) . '"' : '') . '>';
                echo '<head>';
                echo '<meta charset="UTF-8" />';
                echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />';
                echo '<meta http-equiv="X-UA-Compatible" content="IE=edge" />';
                echo '<meta name="robots" content="noindex">';

                if (!empty($description)) {
                    echo '<meta name="description" content="' . esc_attr($description) . '" />';
                }

                if (!empty($keywords)) {
                    echo '<meta name="keywords" content="' . esc_attr($keywords) . '" />';
                }

                echo '<meta property="og:url" content="' . esc_url($url) . '" />';

                if (!empty($form->name)) {
                    echo '<meta property="og:title" content="' . esc_attr($form->name) . '" />';
                }

                if (!empty($description)) {
                    echo '<meta property="og:description" content="' . esc_attr($description) . '" />';
                }

                if (!empty($form->name)) {
                    echo '<title>' . esc_html($form->name) . '</title>';
                }

                echo '</head>';
                echo '<body style="margin:0;overflow:hidden;">';

                echo Runner::script($props);

                echo '</body>';
                echo '</html>';

                die();
            }
        }
    }

    static function register($plugin)
    {
        register_activation_hook($plugin, ['Tripetto\Runner', 'activate']);

        add_action('wp_ajax_tripetto_runner_definition', ['Tripetto\Runner', 'definition']);
        add_action('wp_ajax_nopriv_tripetto_runner_definition', ['Tripetto\Runner', 'definition']);
        add_action('wp_ajax_tripetto_runner_styles', ['Tripetto\Runner', 'styles']);
        add_action('wp_ajax_nopriv_tripetto_runner_styles', ['Tripetto\Runner', 'styles']);
        add_action('wp_ajax_tripetto_runner_l10n', ['Tripetto\Runner', 'l10n']);
        add_action('wp_ajax_nopriv_tripetto_runner_l10n', ['Tripetto\Runner', 'l10n']);
        add_action('wp_ajax_tripetto_runner_snapshot', ['Tripetto\Runner', 'snapshot']);
        add_action('wp_ajax_nopriv_tripetto_runner_snapshot', ['Tripetto\Runner', 'snapshot']);
        add_action('wp_ajax_tripetto_announce', ['Tripetto\Runner', 'announce']);
        add_action('wp_ajax_nopriv_tripetto_announce', ['Tripetto\Runner', 'announce']);
        add_action('wp_ajax_tripetto_submit', ['Tripetto\Runner', 'submit']);
        add_action('wp_ajax_nopriv_tripetto_submit', ['Tripetto\Runner', 'submit']);
        add_action('wp_ajax_tripetto_pause', ['Tripetto\Runner', 'pause']);
        add_action('wp_ajax_nopriv_tripetto_pause', ['Tripetto\Runner', 'pause']);
        add_action('init', ['Tripetto\Runner', 'standalone']);

        add_shortcode('tripetto', ['Tripetto\Runner', 'shortcode']);
    }
}
?>
