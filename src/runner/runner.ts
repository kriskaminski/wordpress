import {
    IDefinition,
    ISnapshot,
    TStyles,
    TL10n,
    L10n,
    Instance,
    Export,
    checksum,
    castToString,
    castToNumber,
    findFirst,
    powSolve,
} from "tripetto-runner-foundation";
import { IRunnerAttachments } from "tripetto-runner-react-hook";
import { CSSProperties } from "react";
import { getLocale, getTranslation } from "../builder/helpers/l10n";
import * as Superagent from "superagent";

interface INamespace {
    readonly run: (props: {
        readonly element?: HTMLElement;
        readonly definition?: IDefinition | Promise<IDefinition | undefined>;
        readonly snapshot?: ISnapshot | Promise<ISnapshot | undefined>;
        readonly styles?: TStyles | Promise<TStyles | undefined>;
        readonly l10n?: TL10n | Promise<TL10n | undefined>;
        readonly language?: string;
        readonly locale: (locale: "auto" | string) => Promise<L10n.ILocale | undefined>;
        readonly translations: (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>;
        readonly onSubmit?: (
            instance: Instance,
            language: string,
            locale: string,
            namespace?: string
        ) => Promise<string | undefined> | boolean | void;
        readonly onPause?: {
            readonly recipe: "email";
            readonly onPause: (
                emailAddress: string,
                snapshot: ISnapshot,
                language: string,
                locale: string,
                namespace: string
            ) => Promise<void> | boolean | void;
        };
        readonly onReload?: () => Promise<IDefinition>;
        readonly display?: "page";
        readonly pausable?: boolean;
        readonly persistent?: boolean;
        readonly attachments?: IRunnerAttachments;
        readonly customCSS?: string;
        readonly customStyle?: CSSProperties;
    }) => void;
}

declare const TripettoAutoscroll: INamespace | undefined;
declare const TripettoChat: INamespace | undefined;
declare const TripettoClassic: INamespace | undefined;

function getNamespace(runner: string): INamespace | undefined {
    switch (runner) {
        case "chat":
            return (typeof TripettoChat !== "undefined" && TripettoChat) || undefined;
        case "classic":
            return (typeof TripettoClassic !== "undefined" && TripettoClassic) || undefined;
        default:
            return (typeof TripettoAutoscroll !== "undefined" && TripettoAutoscroll) || undefined;
    }
}

function getRunner(runner: string): Promise<INamespace> {
    return new Promise((resolve: (namespace: INamespace) => void) => {
        const namespace = getNamespace(runner);

        if (!namespace) {
            const handle = setInterval(() => {
                const n = getNamespace(runner);

                if (n) {
                    clearInterval(handle);

                    resolve(n);
                }
            }, 100);
        } else {
            resolve(namespace);
        }
    });
}

function getSnapshotToken(): string {
    if (window.location.search) {
        const n = window.location.search.indexOf("tripetto-resume=");

        if (n !== -1) {
            const reference = window.location.search.substr(n + 16, 64);

            if (reference.length === 64) {
                return reference;
            }
        }
    }

    return "";
}

export async function run(props: {
    readonly reference: string;
    readonly nonce?: string;
    readonly runner: string;
    readonly element: string;
    readonly url: string;
    readonly language?: string;
    readonly definition?: IDefinition;
    readonly snapshot?: ISnapshot;
    readonly snapshotToken?: string;
    readonly styles?: TStyles;
    readonly l10n?: TL10n;
    readonly fullPage?: boolean;
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly css?: string;
}): Promise<void> {
    const namespace = await getRunner(props.runner);
    let nonce = props.nonce || "";

    const definition =
        props.definition ||
        new Promise<IDefinition | undefined>((resolve: (definition: IDefinition | undefined) => void) => {
            Superagent.post(props.url)
                .type("form")
                .send({
                    action: "tripetto_runner_definition",
                    reference: props.reference,
                })
                .then((response) => {
                    if (response.body && response.body.definition && response.body.nonce) {
                        nonce = response.body.nonce;

                        resolve(response.body.definition);
                    } else {
                        resolve(undefined);
                    }
                })
                .catch(() => resolve(undefined));
        });

    const styles =
        props.styles ||
        new Promise<TStyles | undefined>((resolve: (styles: TStyles | undefined) => void) => {
            Superagent.post(props.url)
                .type("form")
                .send({
                    action: "tripetto_runner_styles",
                    reference: props.reference,
                })
                .then((response) => resolve(response.body))
                .catch(() => resolve(undefined));
        });

    const l10n =
        props.l10n ||
        new Promise<TL10n | undefined>((resolve: (l10n: TL10n | undefined) => void) => {
            Superagent.post(props.url)
                .type("form")
                .send({
                    action: "tripetto_runner_l10n",
                    reference: props.reference,
                })
                .then((response) => resolve(response.body))
                .catch(() => resolve(undefined));
        });

    const snapshotToken = (props.pausable && (props.snapshotToken || getSnapshotToken())) || undefined;
    const snapshot =
        (props.pausable &&
            (props.snapshot ||
                (snapshotToken &&
                    new Promise<ISnapshot | undefined>((resolve: (snapshot: ISnapshot | undefined) => void) => {
                        Superagent.post(props.url)
                            .type("form")
                            .send({
                                action: "tripetto_runner_snapshot",
                                reference: props.reference,
                                token: snapshotToken,
                            })
                            .then((response) => resolve(response.body))
                            .catch(() => resolve(undefined));
                    })))) ||
        undefined;

    namespace.run({
        element: document.getElementById(props.element) || undefined,
        definition,
        snapshot,
        language: props.language,
        l10n,
        styles,
        locale: (locale: "auto" | string) => getLocale(props.url, locale),
        translations: (language: "auto" | string, context: string) => getTranslation(props.url, language, context),
        display: (props.fullPage && "page") || undefined,
        persistent: props.persistent,
        customCSS: props.css,
        customStyle:
            props.width || props.height
                ? {
                      width: props.width,
                      height: props.height,
                  }
                : undefined,
        onSubmit: (instance: Instance, language: string, locale: string) =>
            new Promise((resolve: (id: string | undefined) => void, reject: (reason?: string) => void) => {
                const exportables = Export.exportables(instance);
                const actionables = Export.actionables(instance);

                /**
                 * If we have no fields or only fields without data, we don't need to
                 * post that data to the server. It's simply a form without data.
                 */
                if (!actionables && !findFirst(exportables.fields, (field) => (field.time ? true : false))) {
                    return resolve("");
                }

                const payloadChecksum = checksum({ exportables, actionables }, true);

                if (payloadChecksum) {
                    // First announce a new submission
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_announce",
                            reference: props.reference,
                            nonce,
                            fingerprint: exportables.fingerprint,
                            checksum: payloadChecksum,
                        })
                        .then((announcement) => {
                            // Then perform work and submit the nonce and payload.
                            if (announcement.status === 200 && announcement.body && announcement.body.id && announcement.body.difficulty) {
                                try {
                                    const id = castToString(announcement.body.id);
                                    const difficulty = castToNumber(announcement.body.difficulty);
                                    const timestamp = castToNumber(announcement.body.timestamp);

                                    Superagent.post(props.url)
                                        .type("form")
                                        .send({
                                            action: "tripetto_submit",
                                            id,
                                            nonce: powSolve({ exportables, actionables }, difficulty, id, 16, 1000 * 60 * 5, timestamp),
                                            language,
                                            locale,
                                            exportables: JSON.stringify(exportables),
                                            actionables: JSON.stringify(actionables),
                                            snapshot: snapshotToken,
                                        })
                                        .then((result) => {
                                            if (result.status === 200 && result.text) {
                                                resolve(result.text);
                                            } else {
                                                reject(
                                                    result.status === 403 ? "rejected" : (result.error && result.error.text) || undefined
                                                );
                                            }
                                        })
                                        .catch((result) => reject((result && result.status === 403 && "rejected") || undefined));
                                } catch {
                                    reject();
                                }
                            } else {
                                reject(
                                    announcement.status === 409 ? "outdated" : (announcement.error && announcement.error.text) || undefined
                                );
                            }
                        })
                        .catch((result) => reject((result && result.status === 409 && "outdated") || undefined));
                } else {
                    reject();
                }
            }),
        onPause:
            (props.pausable && {
                recipe: "email",
                onPause: (emailAddress: string, snapshot: ISnapshot, language: string, locale: string, runner: string) =>
                    new Promise<void>((resolve: () => void, reject: (reason?: string) => void) => {
                        Superagent.post(props.url)
                            .type("form")
                            .send({
                                action: "tripetto_pause",
                                reference: props.reference,
                                nonce,
                                url: window.location.href,
                                emailAddress,
                                snapshot: JSON.stringify(snapshot),
                                language,
                                locale,
                                runner,
                                token: snapshotToken,
                            })
                            .then((res) => {
                                if (res.status === 200) {
                                    resolve();
                                } else {
                                    reject((res.error && res.error.text) || undefined);
                                }
                            })
                            .catch(() => Promise.reject());
                    }),
            }) ||
            undefined,
        onReload: () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                Superagent.post(props.url)
                    .type("form")
                    .send({
                        action: "tripetto_runner_definition",
                        reference: props.reference,
                    })
                    .then((response) => {
                        if (response.body && response.body.definition && response.body.nonce) {
                            nonce = response.body.nonce;

                            resolve(response.body.definition);
                        } else {
                            reject();
                        }
                    })
                    .catch(() => reject());
            }),
        attachments: {
            put: (file: File, onProgress?: (percentage: number) => void) =>
                new Promise((resolve: (reference: string) => void, reject: (reason?: string) => void) => {
                    const formData = new FormData();

                    formData.append("action", "tripetto_attachment_upload");
                    formData.append("file", file);
                    formData.append("reference", props.reference);
                    formData.append("nonce", nonce);

                    Superagent.post(props.url)
                        .send(formData)
                        .on("progress", (event: Superagent.ProgressEvent) => {
                            if (event.direction === "upload" && onProgress) {
                                onProgress(event.percent || 0);
                            }
                        })
                        .then((res: Superagent.Response) => {
                            if (res.status === 200 && res.text) {
                                resolve(res.text);
                            } else {
                                reject(res.status === 413 ? "File is too large." : undefined);
                            }
                        })
                        .catch(() => reject());
                }),
            get: (file: string) =>
                new Promise<Blob>((resolve: (data: Blob) => void, reject: () => void) => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_attachment_download",
                            reference: props.reference,
                            nonce,
                            file,
                        })
                        .responseType("blob")
                        .then((res: Superagent.Response) => {
                            if (res.status === 200) {
                                resolve(res.body);
                            } else {
                                reject();
                            }
                        })
                        .catch(() => reject());
                }),
            delete: (file: string) => {
                return new Promise((resolve: () => void, reject: () => void) => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_attachment_unload",
                            reference: props.reference,
                            nonce,
                            file,
                        })
                        .then((res: Superagent.Response) => {
                            if (res.status === 200) {
                                resolve();
                            } else {
                                reject();
                            }
                        })
                        .catch(() => reject());
                });
            },
        },
    });
}
