<?php
namespace Tripetto;

class Attachments
{
    static function activate($network_wide)
    {
        if (!current_user_can('activate_plugins')) {
            return;
        }

        if (is_multisite() && $network_wide) {
            return;
        }

        Attachments::database();

        // Update .htaccess to protect attachments
        if (!function_exists('get_home_path')) {
            include_once ABSPATH . '/wp-admin/includes/file.php';
        }

        if (!function_exists('insert_with_markers')) {
            include_once ABSPATH . '/wp-admin/includes/misc.php';
        }

        $htaccess = get_home_path() . '.htaccess';
        $lines = ['RedirectMatch 401 ^/wp-content/uploads/.*/tripetto/.*$'];

        insert_with_markers($htaccess, 'Tripetto', $lines);
    }

    static function database()
    {
        Database::assert(
            "tripetto_attachments",
            [
                "form_id int(10) unsigned NOT NULL",
                "entry_id int(10) unsigned NULL",
                "reference varchar(65) NOT NULL DEFAULT ''",
                "name text NOT NULL",
                "path text NOT NULL",
                "type tinytext NOT NULL",
                "created datetime NULL DEFAULT NULL",
            ],
            ["form_id", "entry_id", "reference"]
        );
    }

    static function setUploadDir($param)
    {
        $customDir = '/tripetto';

        $param['subdir'] = $param['subdir'] . $customDir;
        $param['path'] = $param['path'] . $customDir;

        return $param;
    }

    static function upload()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $file = $_FILES['file'];

        if (!empty($reference) && !empty($nonce) && isset($file)) {
            global $wpdb;

            $form = $wpdb->get_row(
                $wpdb->prepare("SELECT id,fingerprint from {$wpdb->prefix}tripetto_forms where reference=%s", $reference)
            );

            if (!is_null($form) && wp_verify_nonce($nonce, Runner::runnerNonce($form->id))) {
                $filename = sanitize_file_name($file['name']);

                add_filter('upload_dir', ['Tripetto\Attachments', 'setUploadDir']);

                $uploadDir = wp_upload_dir();

                $wpdb->insert($wpdb->prefix . "tripetto_attachments", [
                    'form_id' => $form->id,
                    'entry_id' => 0,
                    'reference' => '',
                    'name' => $filename,
                    'path' => $uploadDir['path'],
                    'type' => sanitize_mime_type($file['type']),
                    'created' => date("Y-m-d H:i:s"),
                ]);

                $attachmentId = intval($wpdb->insert_id);

                if (!empty($attachmentId)) {
                    $reference = hash("sha256", $form->fingerprint . wp_create_nonce('tripetto:attachments:upload:' . $attachmentId));

                    // Change the filename.
                    $file['name'] = $reference;

                    // Save the file to disk.
                    $uploaded_file = wp_handle_upload($file, [
                        'test_form' => false,
                        'test_type' => false,
                        'action' => 'upload_attachment',
                    ]);

                    if ($uploaded_file && empty($uploaded_file['error'])) {
                        $wpdb->update($wpdb->prefix . "tripetto_attachments", ['reference' => $reference], ['id' => $attachmentId]);

                        echo $reference;
                        http_response_code(200);

                        return die();
                    } else {
                        $wpdb->delete($wpdb->prefix . "tripetto_attachments", [
                            'id' => $attachmentId,
                        ]);
                    }
                }
            }
        }

        http_response_code(500);

        die();
    }

    static function download()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $file = !empty($_POST['file']) ? $_POST['file'] : "";

        if (!empty($reference) && !empty($nonce) && !empty($file)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT id from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            $attachment = $wpdb->get_row(
                $wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_attachments WHERE reference=%s AND entry_id=0", $file)
            );

            if (
                !is_null($form) &&
                wp_verify_nonce($nonce, Runner::runnerNonce($form->id)) &&
                !is_null($attachment) &&
                !empty($attachment->id)
            ) {
                header("content-type: {$attachment->type}");
                readfile($attachment->path . '/' . $attachment->reference);

                return die();
            }
        }

        http_response_code(404);

        die();
    }

    static function unload()
    {
        $reference = !empty($_POST['reference']) ? $_POST['reference'] : "";
        $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
        $file = !empty($_POST['file']) ? $_POST['file'] : "";

        if (!empty($reference) && !empty($nonce) && !empty($file)) {
            global $wpdb;

            $form = $wpdb->get_row($wpdb->prepare("SELECT id from {$wpdb->prefix}tripetto_forms where reference=%s", $reference));

            $attachment = $wpdb->get_row(
                $wpdb->prepare("SELECT id FROM {$wpdb->prefix}tripetto_attachments WHERE reference=%s AND entry_id=0", $file)
            );

            if (
                !is_null($form) &&
                wp_verify_nonce($nonce, Runner::runnerNonce($form->id)) &&
                !is_null($attachment) &&
                !empty($attachment->id)
            ) {
                Attachments::delete($attachment->id);

                http_response_code(200);

                return die();
            }
        }

        http_response_code(404);

        die();
    }

    static function confirm($reference, $entryId)
    {
        if (!empty($reference)) {
            global $wpdb;

            $wpdb->update(
                $wpdb->prefix . "tripetto_attachments",
                ['entry_id' => $entryId],
                [
                    'reference' => $reference,
                    'entry_id' => 0,
                ]
            );
        }
    }

    static function delete($id)
    {
        global $wpdb;

        $attachment = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_attachments WHERE id=%d", $id));

        if (!is_null($attachment)) {
            $wpdb->delete($wpdb->prefix . "tripetto_attachments", [
                'id' => $id,
            ]);

            wp_delete_file($attachment->path . '/' . $attachment->reference);
        }
    }

    static function export()
    {
        if (!empty($_REQUEST['action']) && $_REQUEST['action'] == "tripetto-attachment" && Tripetto::assert()) {
            $reference = !empty($_REQUEST['reference']) ? $_REQUEST['reference'] : "";

            if (!empty($reference)) {
                global $wpdb;

                $attachment = $wpdb->get_row(
                    $wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_attachments WHERE reference=%s AND entry_id>0", $reference)
                );

                // Fallback on id for backwards compatibility with older versions of the plugin.
                if (is_null($attachment)) {
                    $reference = intval($reference);

                    if (!empty($reference)) {
                        $attachment = $wpdb->get_row(
                            $wpdb->prepare("SELECT * FROM {$wpdb->prefix}tripetto_attachments WHERE id=%d AND entry_id>0", $reference)
                        );
                    }
                }

                if (!is_null($attachment)) {
                    header("content-type: $attachment->type");
                    header("content-disposition: inline; filename=$attachment->name");

                    readfile($attachment->path . '/' . $attachment->reference);

                    return die();
                }

                http_response_code(404);
            }

            die();
        }
    }

    static function isAttachment($field)
    {
        return $field->type == "tripetto-block-file-upload" || $field->type == "file-upload";
    }

    static function validate($exportables, $id)
    {
        foreach ($exportables->fields as $field) {
            if (Attachments::isAttachment($field) && !empty($field->reference)) {
                Attachments::confirm($field->reference, $id);
            }
        }
    }

    static function register($plugin)
    {
        add_action('wp_ajax_tripetto_attachment_upload', ['Tripetto\Attachments', 'upload']);
        add_action('wp_ajax_nopriv_tripetto_attachment_upload', ['Tripetto\Attachments', 'upload']);
        add_action('wp_ajax_tripetto_attachment_download', ['Tripetto\Attachments', 'download']);
        add_action('wp_ajax_nopriv_tripetto_attachment_download', ['Tripetto\Attachments', 'download']);
        add_action('wp_ajax_tripetto_attachment_unload', ['Tripetto\Attachments', 'unload']);
        add_action('wp_ajax_nopriv_tripetto_attachment_unload', ['Tripetto\Attachments', 'unload']);

        if (is_admin()) {
            register_activation_hook($plugin, ['Tripetto\Attachments', 'activate']);
            add_action('init', ['Tripetto\Attachments', 'export']);
        }
    }
}
?>
