<?php
namespace Tripetto;

class Mailer
{
    static function send($to, $subject, $message, $replyTo = "")
    {
        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $headers = ['Content-Type: text/html; charset=UTF-8', 'From:' . get_bloginfo('name') . ' <' . get_bloginfo('admin_email') . '>'];

        if (!empty($replyTo) && filter_var($replyTo, FILTER_VALIDATE_EMAIL)) {
            array_push($headers, "Reply-To: {$replyTo}");
        }

        return wp_mail($to, $subject, $message, $headers);
    }
}
?>
