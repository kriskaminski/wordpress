<?php
namespace Tripetto;

class Installation
{
    static function loaded()
    {
        $version = Database::option("tripetto_version");

        if (empty($version) || version_compare($version, $GLOBALS["TRIPETTO_PLUGIN_VERSION"]) < 0) {
            if (empty($version)) {
                add_option("tripetto_version", $GLOBALS["TRIPETTO_PLUGIN_VERSION"], "", false);
            } else {
                update_option("tripetto_version", $GLOBALS["TRIPETTO_PLUGIN_VERSION"], false);
            }

            Forms::database();
            Results::database();
            Attachments::database();
            Runner::database();

            if (!empty($version)) {
                add_option("tripetto_upgraded", "yes", "", false);

                License::upgrade();
                Migration::apply($version);
            }
        }
    }

    static function uninstall()
    {
        global $wpdb;

        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_forms");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_announcements");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_snapshots");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_entries");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tripetto_attachments");

        delete_option("tripetto_version");
        delete_option("tripetto_db");
        delete_option("tripetto_upgraded");
        delete_option("tripetto_notify_free_premium");
    }

    static function notification()
    {
        if (!version_compare(PHP_VERSION, '5.6.20', '>=')) {
            echo '<div class="notice notice-error">';
            echo '<p><strong>Tripetto requires PHP version 5.6.20 or above!</strong></p>';
            echo '</div>';
        }

        $screenId = get_current_screen()->id;

        if (
            is_admin() &&
            is_user_logged_in() &&
            ($screenId == "toplevel_page_tripetto" || ($screenId == "tripetto_page_tripetto-forms" && empty($_REQUEST['action']))) &&
            !empty(get_option("tripetto_upgraded"))
        ) {
            delete_option("tripetto_upgraded");

            echo '<div class="notice notice-success is-dismissible">';
            echo '<p>🎉 <strong>' .
                /* translators: %s is replaced with the current version number of Tripetto */
                sprintf(__('Welcome to Tripetto version %s', 'tripetto'), $GLOBALS["TRIPETTO_PLUGIN_VERSION"]) .
                '</strong><br />' .
                __("Thank you for upgrading!", 'tripetto') .
                '<br />' .
                __("Want to know what's new?", 'tripetto') .
                ' <a href="https://gitlab.com/tripetto/wordpress/-/blob/master/CHANGELOG" target="_blank">' .
                __("Check out our changelog", 'tripetto') .
                '</a>.' .
                '<br />' .
                __("Want to help improve the product and spread the word?", 'tripetto') .
                ' <a href="https://wordpress.org/plugins/tripetto/#reviews" target="_blank">' .
                __("Rate/review us on WordPress", 'tripetto') .
                '</a>.' .
                '</p>';
            echo '</div>';
        }
    }

    static function register($plugin)
    {
        add_action('wp_loaded', ['Tripetto\Installation', 'loaded']);
        add_action('admin_notices', ['Tripetto\Installation', 'notification']);
        tripetto_freemius()->add_action('after_uninstall', ['Tripetto\Installation', 'uninstall']);
    }
}
?>
