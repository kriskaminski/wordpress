<?php
namespace Tripetto;

class Helpers
{
    static function pluginUrl()
    {
        return rtrim(plugin_dir_url(dirname(__FILE__)), "/");
    }

    static function isValidJSON($json)
    {
        if (!isset($json) || empty($json)) {
            return false;
        }

        $result = json_decode($json, false);

        return json_last_error() == JSON_ERROR_NONE && is_object($result);
    }

    static function isValidSHA256($hash)
    {
        if (!empty($hash) && strlen($hash) == 64 && preg_match("/^([a-f0-9]{64})$/", $hash) == 1) {
            return true;
        }

        return false;
    }
}
?>
