<?php
namespace Tripetto;

class Translation
{
    static function translationVerify($locale, $context)
    {
        if (empty($locale) || !preg_match('/^[A-Za-z]{2,4}([_-]([A-Za-z]{4}|[0-9]{3}))?([_-]([A-Za-z]{2}|[0-9]{3}))?$/', $locale)) {
            return '';
        }

        $data = @file_get_contents(dirname(__DIR__) . '/languages/' . (!empty($context) ? $context . '-' : '') . $locale . '.json');

        if (!empty($data)) {
            return $data;
        }

        $i = strpos($locale, '_');

        if ($i !== false) {
            $locale = substr($locale, 0, $i);
            $data = @file_get_contents(dirname(__DIR__) . '/languages/' . (!empty($context) ? $context . '-' : '') . $locale . '.json');

            if (!empty($data)) {
                return $data;
            }
        }

        return '';
    }

    static function translationData($locale, $context)
    {
        if (!empty($context) && !preg_match('/^[a-z-]+$/', $context)) {
            return '';
        }

        if ($locale == 'en') {
            return '';
        }

        if (!empty($locale)) {
            $i = strpos($locale, ';');

            if ($i !== false) {
                $locale = substr($locale, 0, $i);
            }

            $i = strpos($locale, ',');

            if ($i !== false) {
                $locale = substr($locale, 0, $i);
            }

            $data = Translation::translationVerify($locale, $context);

            if (!empty($data)) {
                return $data;
            }
        }

        return '';
    }

    static function getTranslation()
    {
        $language = !empty($_POST['language']) ? $_POST['language'] : get_locale();
        $context = !empty($_POST['context']) ? $_POST['context'] : 'tripetto';

        $data = Translation::translationData(str_replace('-', '_', $language), $context);

        if ($data != "") {
            header('Content-Type: application/json');

            echo $data;
        }

        die();
    }

    static function loadTranslation()
    {
        load_plugin_textdomain('tripetto', false, dirname(dirname(plugin_basename(__FILE__))) . '/languages/');
    }

    static function register($plugin)
    {
        add_action('wp_ajax_tripetto_translation', ['Tripetto\Translation', 'getTranslation']);
        add_action('wp_ajax_nopriv_tripetto_translation', ['Tripetto\Translation', 'getTranslation']);
        add_action('init', ['Tripetto\Translation', 'loadTranslation']);
    }
}
?>
