<?php
namespace Tripetto;

class License
{
    /**
     * Returns whether a form has premium features.
     *
     * @param array $formId The id of the form.
     */
    static function hasPremiumFeatures($formId)
    {
        if (License::isInPremiumMode() || !empty(get_option("tripetto_" . hash("sha256", "premium" . strval($formId))))) {
            return true;
        }

        return false;
    }

    /**
     * Returns whether the plugin is in premium mode.
     */
    static function isInPremiumMode()
    {
        if (tripetto_freemius()->can_use_premium_code__premium_only()) {
            return true;
        }

        return false;
    }

    static function upgrade()
    {
        if (!License::isInPremiumMode()) {
            global $wpdb;

            $freePremium = $wpdb->get_results(
                $wpdb->prepare(
                    "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s ",
                    DB_NAME,
                    $wpdb->prefix . "tripetto_forms",
                    "premium"
                )
            );

            if (!empty($freePremium)) {
                $premiumForm = $wpdb->get_var("SELECT id from {$wpdb->prefix}tripetto_forms where premium > 0 ORDER BY id LIMIT 1");

                if (!is_null($premiumForm) && !empty($premiumForm)) {
                    add_option("tripetto_notify_free_premium", "yes", "", false);
                    add_option("tripetto_" . hash("sha256", "premium" . strval($premiumForm)), "premium", "", false);
                }
            }
        }
    }

    static function notification()
    {
        $screenId = get_current_screen()->id;

        if (
            is_admin() &&
            is_user_logged_in() &&
            $screenId == "tripetto_page_tripetto-forms" &&
            empty($_REQUEST['action']) &&
            !empty(get_option("tripetto_notify_free_premium"))
        ) {
            delete_option("tripetto_notify_free_premium");

            echo '<div class="notice notice-warning is-dismissible">';
            echo '<p>💥 <strong>' .
                __('Important message for you', 'tripetto') .
                '</strong><br />' .
                __(
                    "Please note that this latest free version of Tripetto does no longer come with one free premium form for new users. But you are not a new user! You were using Tripetto before this big upgrade and we are very grateful for that. That’s why the form you designated as premium form will keep all its premium features going forward. You can recognize it by the premium badge in your list of forms.",
                    'tripetto'
                ) .
                '</p>';
            echo '</div>';
        }
    }

    static function register($plugin)
    {
        add_action('admin_notices', ['Tripetto\License', 'notification']);
    }
}
?>
