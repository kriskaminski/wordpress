export interface IHooks {
    email?: {
        enabled: boolean;
        recipient?: string;
        includeFields?: boolean;
    };

    slack?: {
        enabled: boolean;
        url?: string;
        includeFields?: boolean;
    };

    webhook?: {
        enabled: boolean;
        url?: string;
        nvp?: boolean;
    };
}
