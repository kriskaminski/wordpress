import { Components, Debounce, Forms, Layers, _, REGEX_IS_URL } from "tripetto";
import { BuilderComponent } from "../builder";
import superagent from "superagent";
import { IHooks } from "./hooks";
import { REGEX_IS_EMAIL } from "../../helpers/regex";
import { HELP_NOTIFICATION, HELP_SLACK, HELP_WEBHOOK } from "../../urls";

export class AutomateComponent extends Components.Controller<{
    readonly id: number;
    readonly premium: boolean;
    readonly hooks: IHooks;
}> {
    private readonly builder: BuilderComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(builder: BuilderComponent, hooks: IHooks, id: number, premium: boolean): AutomateComponent {
        return builder.openPanel(
            (panel: Layers.Layer) => new AutomateComponent(panel, builder, hooks, id, premium),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, builder: BuilderComponent, hooks: IHooks, id: number, premium: boolean) {
        super(
            layer,
            {
                id,
                hooks,
                premium,
            },
            _("Automate"),
            "compact",
            builder.style,
            "right",
            "on-when-validated",
            _("Close")
        );

        this.builder = builder;

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testSlack(url: string, includeFields: boolean): Promise<boolean> {
        return new Promise((resolve: (succeeded: boolean) => void) => {
            superagent
                .post(this.builder.ajaxUrl)
                .type("form")
                .send({ action: "tripetto_test_slack", url, includeFields })
                .then((res) => {
                    resolve(res.status === 200);
                })
                .catch(() => {
                    resolve(false);
                });
        });
    }

    private testWebhook(url: string, nvp: boolean): Promise<boolean> {
        return new Promise((resolve: (succeeded: boolean) => void) => {
            superagent
                .post(this.builder.ajaxUrl)
                .type("form")
                .send({ action: "tripetto_test_webhook", url, nvp })
                .then((res) => {
                    resolve(res.status === 200);
                })
                .catch(() => {
                    resolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const updateSettings = new Debounce(() => {
            this.builder.post({
                action: "tripetto_hooks",
                id: this.ref.id,
                hooks: JSON.stringify({ email: emailSettings, slack: slackSettings, webhook: webhookSettings }),
            });
        }, 300);

        if (!this.ref.premium) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            _("A premium subscription is required to be able to use all the automation features."),
                            "info"
                        ),
                    ],
                })
            );
        }

        const emailSettings = this.ref.hooks.email || (this.ref.hooks.email = { enabled: false, recipient: "" });
        const emailSettingsGroup = new Forms.Group([]);

        const emailIncludeFields = new Forms.Checkbox(_("Include response data in the message"), emailSettings.includeFields).on(
            (checkbox: Forms.Checkbox) => {
                emailSettings.includeFields = checkbox.isChecked;
                updateSettings.invoke();
            }
        );
        const emailRecipientLabel = new Forms.Static(_("Send a notification to:"));
        const emailRecipient = new Forms.Email((emailSettings && emailSettings.recipient) || "")
            .autoValidate((email: Forms.Text) =>
                email.value === "" || email.isDisabled || !email.isObservable
                    ? "unknown"
                    : REGEX_IS_EMAIL.test(email.value)
                    ? "pass"
                    : "fail"
            )
            .on((email: Forms.Email) => {
                if (emailSettings && !email.isInvalid) {
                    emailSettings.recipient = email.value;

                    updateSettings.invoke();
                }
            });
        const optionEmail = new Forms.Checkbox(
            _("Send an email when someone completes your form ([learn more](%1))", HELP_NOTIFICATION),
            emailSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;
                emailSettings.enabled = checked;
                updateSettings.invoke();

                emailSettingsGroup.visible(checked);
            });

        emailSettingsGroup.controls.push(emailRecipientLabel, emailRecipient, emailIncludeFields);
        emailSettingsGroup.visible(optionEmail.isChecked);

        cards.add(
            new Forms.Form({
                title: _("📧 Email notification"),
                controls: [optionEmail, emailSettingsGroup],
            })
        );

        const slackSettings = this.ref.hooks.slack || (this.ref.hooks.slack = { enabled: false });
        const slackSettingsGroup = new Forms.Group([]);
        const slackIncludeFields = new Forms.Checkbox(_("Include response data in the message"), slackSettings.includeFields).on(
            (checkbox: Forms.Checkbox) => {
                slackSettings.includeFields = checkbox.isChecked;
                updateSettings.invoke();
            }
        );
        const slackUrl = new Forms.Text("singleline", slackSettings.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((text: Forms.Text) => {
                slackSettings.url = text.value;
                updateSettings.invoke();

                slackTestButton.buttonType = "normal";
                slackTestButton.disabled(!slackUrl.value || !REGEX_IS_URL.test(slackUrl.value));
                slackTestButton.label(_("🩺 Test"));
            });
        const slackTestButton = new Forms.Button(_("🩺 Test")).on((testButton) => {
            slackUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label(_("⏳ Testing..."));

            this.testSlack(slackUrl.value, slackIncludeFields.isChecked).then((succeeded) => {
                slackUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? _("✔ All good!") : _("❌ Something's wrong"));
            });
        });

        const optionSlack = new Forms.Checkbox(
            _("Send a Slack message when someone completes your form ([learn more](%1))", HELP_SLACK),
            slackSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                slackSettings.enabled = checked;
                updateSettings.invoke();

                slackSettingsGroup.visible(checked);
            });

        slackSettingsGroup.controls.push(
            new Forms.Static(
                _(
                    "Please create an incoming [Slack webhook](https://api.slack.com/incoming-webhooks) and enter the URL of the webhook here:"
                )
            ).markdown(),
            slackUrl,
            slackIncludeFields,
            slackTestButton
        );
        slackSettingsGroup.visible(optionSlack.isChecked);

        cards.add(
            new Forms.Form({
                title: _("📣 Slack notification"),
                controls: [optionSlack, slackSettingsGroup],
            })
        ).isDisabled = !this.ref.premium;

        const webhookSettings = this.ref.hooks.webhook || (this.ref.hooks.webhook = { enabled: false, nvp: true });
        const webhookSettingsGroup = new Forms.Group([]);
        const webhookRaw = new Forms.Checkbox(_("Send raw response data to webhook"), !webhookSettings.nvp)
            .description(
                _(
                    "This option sends way more data to your webhook. Unfortunately most webhooks (like Zapier, Integromat, etc.) don't like that and want plain name-value pairs. In that case you should keep this option disabled."
                )
            )
            .on((checkbox: Forms.Checkbox) => {
                webhookSettings.nvp = !checkbox.isChecked;
                updateSettings.invoke();
            });

        const webhookTestButton = new Forms.Button(_("🩺 Test"));
        const webhookUrlLabel = new Forms.Static(_("Post the data to the following URL:"));
        const webhookUrl = new Forms.Text("singleline", webhookSettings.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((text: Forms.Text) => {
                webhookSettings.url = text.value;
                updateSettings.invoke();

                webhookTestButton.buttonType = "normal";
                webhookTestButton.disabled(!webhookUrl.value || !REGEX_IS_URL.test(webhookUrl.value));
                webhookTestButton.label(_("🩺 Test"));
            });
        webhookTestButton.on((testButton) => {
            webhookUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label(_("⏳ Testing..."));

            this.testWebhook(webhookUrl.value, !webhookRaw.isChecked).then((succeeded) => {
                webhookUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? _("✔ All good!") : _("❌ Something's wrong"));
            });
        });

        const optionWebhook = new Forms.Checkbox(
            _("Notify an external service when someone completes your form ([learn more](%1))", HELP_WEBHOOK),
            webhookSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                webhookSettings.enabled = checked;
                updateSettings.invoke();

                webhookSettingsGroup.visible(checked);
            });

        webhookSettingsGroup.controls.push(webhookUrlLabel, webhookUrl, webhookRaw, webhookTestButton);
        webhookSettingsGroup.visible(optionWebhook.isChecked);

        cards.add(
            new Forms.Form({
                title: _("🔗 Webhook"),
                controls: [optionWebhook, webhookSettingsGroup],
            })
        ).isDisabled = !this.ref.premium;
    }
}
