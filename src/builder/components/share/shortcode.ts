export interface IShortcode {
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly fullPage?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly css?: string;
}
