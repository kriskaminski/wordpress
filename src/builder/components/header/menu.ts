import { Components, DOM, linearicon, _ } from "tripetto";
import { BuilderComponent } from "../builder";
import * as URLS from "../../urls";

export class Menu<T> extends Components.ToolbarMenu<T> {
    private icon: number;

    constructor(builder: BuilderComponent, icon: number, fnMenu: () => Components.MenuOption[]) {
        super(builder.style.header.menu, undefined, fnMenu);

        this.icon = icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}

export function supportMenu(builder: BuilderComponent): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe933, _("Help center"), URLS.HELP),
        new Components.MenuLinkWithIcon(0xe627, _("Examples"), URLS.EXAMPLES),
        new Components.MenuLinkWithIcon(0xe6f9, _("Tutorials"), URLS.TUTORIALS),
        new Components.MenuItemWithIcon(0xe712, _("Cheatsheet"), () => builder.openCheatsheet()),
        new Components.MenuLinkWithIcon(0xe90a, _("Report issue/bug"), URLS.ISSUES),
        new Components.MenuLinkWithIcon(0xe695, _("Receive updates"), URLS.SUBSCRIBE),
    ];
}

export function contactMenu(): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe8ae, _("Follow us"), URLS.TWITTER),
        new Components.MenuLinkWithIcon(0xe777, _("Contact us"), URLS.CONTACT),
    ];
}
