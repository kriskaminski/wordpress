import { Components, Layers, _ } from "tripetto";
import { Title } from "./title";
import { Menu, contactMenu, supportMenu } from "./menu";
import { Toggle } from "./toggle";
import { Separator } from "./separator";
import { Spacer } from "./spacer";
import { Button, ButtonWithMenu } from "./button";
import { BuilderComponent } from "../builder";
import { DeviceSize, windowSize } from "../../helpers/device";
import { HELP_RUNNERS } from "../../urls";

export class HeaderComponent {
    private readonly parent: BuilderComponent;
    private readonly support: [Menu<DeviceSize>, Separator<DeviceSize>];
    private readonly builder: [
        ButtonWithMenu<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        Spacer<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Spacer<DeviceSize>,
        Separator<DeviceSize>
    ];
    private readonly preview: {
        desktop: Toggle<DeviceSize>;
        tablet: Toggle<DeviceSize>;
        phone: Toggle<DeviceSize>;
    };

    constructor(parent: BuilderComponent) {
        const style = parent.style.header;
        const title = new Title(style, parent.builder.name || _("Unnamed"), () => this.doEdit());

        this.parent = parent;
        this.preview = {
            desktop: new Toggle<DeviceSize>({
                style,
                icon: 0xe7af,
                onToggle: () => this.toggleDevice("desktop"),
            })
                .excludeInViews("xs", "s", "m")
                .selected(this.parent.device === "desktop"),
            tablet: new Toggle<DeviceSize>({
                style,
                icon: 0xe7ab,
                onToggle: () => this.toggleDevice("tablet"),
            })
                .excludeInViews("xs", "s")
                .selected(this.parent.device === "tablet"),
            phone: new Toggle<DeviceSize>({
                style,
                icon: 0xe7a5,
                onToggle: () => this.toggleDevice("phone"),
            }).selected(this.parent.device === "phone"),
        };

        this.builder = [
            new ButtonWithMenu<DeviceSize>({
                style,
                mode: "customize",
                icon: 0xe62b,
                label: _("Customize"),
                options: () => [
                    ...this.getRunnersMenu(),
                    new Components.MenuSeparator(),
                    new Components.MenuLabel(_("Form appearance")),
                    new Components.MenuItemWithIcon(0xe756, _("Properties"), () => this.doEdit()),
                    new Components.MenuItemWithIcon(0xe61f, _("Styles"), () => this.doStyles()),
                    new Components.MenuItemWithIcon(0xe886, _("Translations"), () => this.doL10n()),
                ],
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "share",
                icon: 0xe8c1,
                label: _("Share"),
                onTap: () => this.doShareOrEmbed(),
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "automate",
                icon: 0xe920,
                label: _("Automate"),
                onTap: () => this.doAutomate(),
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "results",
                icon: 0xe6ac,
                label: _("Results"),
                onTap: () => this.doResults(),
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Spacer(style),
            this.preview.desktop,
            this.preview.tablet,
            this.preview.phone,
            new Spacer(style),
            new Separator<DeviceSize>(style),
        ];

        this.support = [
            new Menu<DeviceSize>(parent, 0xe7ed, () => this.getSupport()).excludeInViews("xs", "s"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s"),
        ];

        const toolbarComponent = parent.layer.component(
            new Components.ToolbarComponent<DeviceSize>(
                {
                    style: style.style,
                    view: windowSize(),
                    left: [
                        new Menu<DeviceSize>(parent, 0xe92b, () => this.getMenu()).includeInViews("xs", "s", "m"),
                        new Separator<DeviceSize>(style).includeInViews("xs", "s", "m"),
                        new Button<DeviceSize>({ style, icon: 0xe93b, onTap: () => this.goBack() }).excludeInViews("xs", "s", "m"),
                        new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
                        title,
                        new Button<DeviceSize>({ style, mode: "small", icon: 0xe672, onTap: () => this.doEdit() }).excludeInViews("xs"),
                    ],
                    right: [
                        new Separator(style),
                        ...this.builder,
                        ...this.support,
                        new Button<DeviceSize>({ style, icon: 0xe94c, onTap: () => this.doFullscreen() }),
                    ],
                },
                Layers.Layer.configuration.height(style.height).alignTop()
            )
        );

        toolbarComponent.layer.hook("OnResize", "synchronous", () => {
            toolbarComponent.toolbar.view = windowSize();
        });

        parent.builder.hook("OnRename", "synchronous", () => (title.label = parent.builder.name || _("Unnamed")));
    }

    private getRunnersMenu(): Components.MenuOption[] {
        return [
            new Components.MenuLabel(_("Form face")),
            new Components.MenuItemWithIcon(
                this.parent.runner === "autoscroll" ? 0xe999 : 0xe98d,
                _("Autoscroll"),
                () => (this.parent.runner = "autoscroll")
            ),
            new Components.MenuItemWithIcon(
                this.parent.runner === "chat" ? 0xe999 : 0xe98d,
                _("Chat"),
                () => (this.parent.runner = "chat")
            ),
            new Components.MenuItemWithIcon(
                this.parent.runner === "classic" ? 0xe999 : 0xe98d,
                _("Classic"),
                () => (this.parent.runner = "classic")
            ),
            new Components.MenuSeparator(),
            new Components.MenuLinkWithIcon(0xe933, _("Help with form faces"), HELP_RUNNERS),
        ];
    }

    private getMenu(): Components.MenuOption[] {
        const options: Components.MenuOption[] = [];

        options.push(new Components.MenuLabel(_("Navigation")), new Components.MenuItemWithIcon(0xe93b, _("Back"), () => this.goBack()));

        options.push(
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Design")),
            new Components.MenuSubmenuWithIcon(0xe6f3, _("Form face"), this.getRunnersMenu()),
            new Components.MenuItemWithIcon(0xe672, _("Properties"), () => this.doEdit()),
            new Components.MenuItemWithIcon(0xe61f, _("Styles"), () => this.doStyles()),
            new Components.MenuItemWithIcon(0xe886, _("Translations"), () => this.doL10n()),
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Run")),
            new Components.MenuItemWithIcon(0xe8c1, _("Share"), () => this.doShareOrEmbed(), this.builder[2].isDisabled),
            new Components.MenuItemWithIcon(0xe920, _("Automate"), () => this.doAutomate(), this.builder[4].isDisabled),
            new Components.MenuItemWithIcon(0xe6ac, _("Results"), () => this.doResults(), this.builder[6].isDisabled)
        );

        if (this.support[0].isExcluded) {
            options.push(
                new Components.MenuSeparator(),
                new Components.MenuLabel(_("More...")),
                new Components.MenuSubmenuWithIcon(0xe7ed, _("Support"), supportMenu(this.parent)),
                new Components.MenuSubmenuWithIcon(0xe699, _("Contact"), contactMenu())
            );
        }

        return options;
    }

    private getSupport(): Components.MenuItem[] {
        return [
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Support")),
            ...supportMenu(this.parent),
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Contact")),
            ...contactMenu(),
        ];
    }

    private goBack(): void {
        this.parent.close();
    }

    private doEdit(): void {
        this.parent.edit();
    }

    private doStyles(): void {
        this.parent.stylesEditor();
    }

    private doL10n(): void {
        this.parent.l10nEditor();
    }

    private doShareOrEmbed(): void {
        const shareButton = this.builder[2];

        shareButton.disable();

        this.parent.shareOrEmbed().whenClosed = () => shareButton.enable();
    }

    private doAutomate(): void {
        const automateButton = this.builder[4];

        automateButton.disable();

        this.parent.automate().whenClosed = () => automateButton.enable();
    }

    private doResults(): void {
        window.parent.postMessage(
            {
                type: "results",
            },
            window.location.origin
        );
    }

    private doFullscreen(): void {
        window.parent.postMessage(
            {
                type: "fullscreen",
            },
            window.location.origin
        );
    }

    private toggleDevice(device: "phone" | "tablet" | "desktop"): void {
        this.parent.toggleDevice(device);
    }

    update(): void {
        this.preview.desktop.isSelected = this.parent.device === "desktop";
        this.preview.tablet.isSelected = this.parent.device === "tablet";
        this.preview.phone.isSelected = this.parent.device === "phone";
    }
}
