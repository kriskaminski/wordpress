import { Components, DOM, linearicon, _ } from "tripetto";
import { IHeaderStyle } from "./style";
import { DeviceSize } from "../../helpers/device";

export class Title extends Components.ToolbarStatic<DeviceSize> {
    constructor(style: IHeaderStyle, title: string, fnTapped: () => void) {
        super(style.title, title, undefined, fnTapped);
    }

    protected onViewChange(): boolean {
        if (this.element) {
            this.element.selectorSafe("compact", this.view === "xs");

            return true;
        }

        return false;
    }

    onDraw(toolbar: Components.Toolbar<DeviceSize>, element: DOM.Element): void {
        element.create("i", (icon: DOM.Element) => linearicon(0xe62d, icon, true));
        element.create("span", (prefix: DOM.Element) => (prefix.text = _("Build")));

        super.onDraw(toolbar, element, element.create("span"));
    }
}
