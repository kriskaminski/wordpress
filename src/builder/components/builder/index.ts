import { DialogComponent } from "../dialog";
import { Builder, Components, Debounce, IDefinition, Layers, _, assert, set } from "tripetto";
import { TL10n, TStyles, calculateFingerprintAndStencil } from "tripetto-runner-foundation";
import { IBuilderProps } from "../../props";
import { IBuilderStyle } from "./style";
import { Runners, TRunners } from "../../helpers/runners";
import { getTranslation } from "../../helpers/l10n";
import { windowSize } from "../../helpers/device";
import { HeaderComponent } from "../header";
import { PreviewComponent, TPreviewDevice } from "../preview";
import { ShareComponent } from "../share";
import { AutomateComponent } from "../automate";
import { Loader } from "../loader";
import { IHooks } from "../automate/hooks";
import { IShortcode } from "../share/shortcode";
import { HELP_L10N, HELP_STYLES } from "../../urls";
import * as Superagent from "superagent";

export class BuilderComponent extends Layers.LayerComponent {
    private header!: HeaderComponent;
    private currentRunner: TRunners;
    private readonly updateDefinition: Debounce;
    private readonly updateStyles: Debounce;
    private readonly updateL10n: Debounce;
    readonly baseUrl: string;
    readonly ajaxUrl: string;
    readonly shareUrl: string;
    readonly version: string;
    readonly style: IBuilderStyle;
    builder!: Builder;
    builderLayer: Layers.Layer | undefined;
    preview!: PreviewComponent;
    readonly language: string;
    readonly id: number;
    readonly styles: TStyles;
    readonly l10n: TL10n;
    readonly tier: "standard" | "premium";
    readonly definition: IDefinition | undefined;
    readonly hooks: IHooks;
    readonly shortcode: IShortcode;
    readonly open?: "share" | "styles" | "l10n" | "automate";
    device: TPreviewDevice;
    whenReady?: () => void;
    whenClosed?: () => void;

    constructor(style: IBuilderStyle, props: IBuilderProps) {
        super(Layers.Layer.configuration.applicationRole());

        const size = windowSize();

        this.style = style;
        this.ajaxUrl = props.ajaxUrl;
        this.baseUrl = props.baseUrl;
        this.shareUrl = props.shareUrl;
        this.version = props.version;
        this.language = props.language;
        this.id = props.id;
        this.definition = props.definition;
        this.currentRunner = this.validateRunner(props.runner);
        this.styles = props.styles || {};
        this.l10n = props.l10n || {};
        this.hooks = props.hooks || {};
        this.shortcode = props.shortcode || {};
        this.tier = props.tier || "standard";
        this.open = props.open;
        this.device = size === "xs" || size === "s" ? "off" : size === "xl" ? "tablet" : "phone";

        this.updateDefinition = new Debounce((id: number, name: string, definition: IDefinition) => {
            const hashes = calculateFingerprintAndStencil(definition);

            this.post({
                action: "tripetto_definition",
                id,
                name,
                definition: JSON.stringify(definition),
                fingerprint: hashes.fingerprint,
                stencil: hashes.stencil("exportables"),
                actionables: hashes.stencil("actionables"),
            });
        }, 300);

        this.updateStyles = new Debounce((id: number, styles: TStyles) => {
            this.post({
                action: "tripetto_styles",
                id,
                styles: JSON.stringify(styles),
            });
        }, 300);

        this.updateL10n = new Debounce((id: string, l10n: TL10n) => {
            this.post({
                action: "tripetto_l10n",
                id,
                l10n: JSON.stringify(l10n),
            });
        }, 300);

        Loader.builder = this;
        DialogComponent.builder = this;

        window.addEventListener("unload", () => {
            this.updateDefinition.flush();
            this.updateStyles.flush();
            this.updateL10n.flush();
        });
    }

    get runner(): TRunners {
        return this.currentRunner;
    }

    set runner(runner: TRunners) {
        Loader.show();

        this.builder
            .useNamespace(runner, Runners.builderBundle(this, runner))
            .then(() => {
                this.preview.changeRunner((this.currentRunner = runner));

                Superagent.post(this.ajaxUrl)
                    .type("form")
                    .send({
                        action: "tripetto_runner",
                        id: this.id,
                        runner: runner,
                    })
                    .then((res) => {
                        Loader.hide();

                        if (!res.ok) {
                            this.showError();
                        }
                    })
                    .catch(() => {
                        Loader.hide();

                        this.showError();
                    });
            })
            .catch(() => Loader.hide());
    }

    private showError(): void {
        DialogComponent.alert(
            _("🤯 Something’s cracking"),
            _(
                "Your actions aren't being processed properly and probably not saved to the server. We apologize for the inconvenience. Please try again and drop a line if troubles persist."
            )
        );
    }

    private validateRunner(runner: string): TRunners {
        switch (runner) {
            case "chat":
                return "chat";
            case "classic":
            case "standard-bootstrap":
                return "classic";
            default:
                return "autoscroll";
        }
    }

    onRender(): void {
        this.openPanel((layer: Layers.Layer) => {
            this.builderLayer = layer;
            this.builder = Builder.open(this.definition, {
                layer,
                layerConfiguration: Layers.Layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device)),
                style: this.style,
                controls: "right",
                fonts: this.baseUrl + "/fonts/",
                zoom: "fit",
                disableLogo: true,
                disableSaveButton: true,
                disableEditButton: true,
                disableCloseButton: true,
                disableTutorialButton: true,
                disableOpenCloseAnimation: true,
                supportURL: false,
                namespace: {
                    identifier: this.currentRunner,
                    umd: Runners.builderBundle(this, this.currentRunner),
                },
                onChange: (changedDefinition: IDefinition) => this.updateDefinition.invoke(this.id, this.builder.name, changedDefinition),
                onPreview: (previewDefinition: IDefinition) => {
                    this.preview.setDefinition(previewDefinition);
                },
                onClose: () => {
                    if (this.whenClosed) {
                        this.whenClosed();
                    }
                },
            });

            this.header = new HeaderComponent(this);
            this.preview = layer.component(new PreviewComponent(this, "preview"));

            layer.hook("OnShow", "framed", () => {
                if (this.whenReady) {
                    this.whenReady();
                }

                switch (this.open) {
                    case "share":
                        this.shareOrEmbed();
                        break;
                    case "styles":
                        this.stylesEditor();
                        break;
                    case "l10n":
                        this.l10nEditor();
                        break;
                    case "automate":
                        this.automate();
                        break;
                }
            });

            const fnTitle = () => {
                parent.document.title = (this.builder.name || _("Unnamed")) + " - Tripetto";
            };

            this.builder.hook("OnRename", "framed", fnTitle);
        });
    }

    openPanel<T>(render: (layer: Layers.Layer) => T, config?: Layers.LayerConfiguration): T {
        let ref!: T;

        (
            this.builderLayer ||
            assert(
                this.layer.createChain(
                    Layers.Layer.configuration
                        .top(this.style.header.height)
                        .layout("habc")
                        .style({
                            layer: {
                                appearance: {
                                    backgroundColor: this.style.background,
                                },
                            },
                            applyToChildren: false,
                        })
                )
            )
        ).createPanel((panel: Layers.Layer) => {
            ref = render(panel);
        }, config);

        return ref;
    }

    post(data: {}): void {
        Superagent.post(this.ajaxUrl)
            .type("form")
            .send(data)
            .then((res) => {
                if (!res.ok) {
                    this.showError();
                }
            })
            .catch(() => {
                this.showError();
            });
    }

    edit(): void {
        this.builder.edit("properties");
    }

    close(): void {
        window.parent.postMessage(
            {
                type: "close",
            },
            window.location.origin
        );
    }

    stylesEditor(): Components.StylesEditor | undefined {
        const contract = Runners.stylesContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.openPanel(
                    (panel: Layers.Layer) =>
                        new Components.StylesEditor(
                            panel,
                            contract,
                            this.styles,
                            this.tier,
                            (styles) => {
                                set(this, "styles", styles);

                                this.preview.setStyles(styles);
                                this.updateStyles.invoke(this.id, styles);
                            },
                            false,
                            true,
                            (done: (bReset: boolean) => void) => {
                                DialogComponent.confirm(
                                    _("Reset styles"),
                                    _("Are you sure you want to reset the styles for this form?"),
                                    _("Yes, reset it!"),
                                    _("No"),
                                    true,
                                    () => done(true),
                                    () => done(false)
                                );
                            },
                            undefined,
                            this.style,
                            "right",
                            [new Components.ToolbarLink(this.style.help, HELP_STYLES)]
                        ),
                    Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
                )
            )
        );
    }

    l10nEditor(): Components.L10nEditor | undefined {
        const contract = Runners.l10nContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.openPanel(
                    (panel: Layers.Layer) =>
                        new Components.L10nEditor(
                            panel,
                            contract,
                            this.builder,
                            this.l10n,
                            (l10n, current) => {
                                set(this, "l10n", l10n);

                                this.preview.setL10n(current);
                                this.updateL10n.invoke(this.id, l10n);
                            },
                            (language: string) => getTranslation(this.ajaxUrl, language, `tripetto-runner-${this.runner}`),
                            undefined,
                            this.style,
                            "right",
                            [new Components.ToolbarLink(this.style.help, HELP_L10N)]
                        ),
                    Layers.Layer.configuration.width(600).animation(Layers.LayerAnimations.Zoom)
                )
            )
        );
    }

    shareOrEmbed(): ShareComponent {
        return ShareComponent.open(this, this.id, this.shareUrl, this.shortcode);
    }

    automate(): AutomateComponent {
        return AutomateComponent.open(this, this.hooks, this.id, this.tier === "premium");
    }

    toggleDevice(device: "phone" | "tablet" | "desktop"): boolean {
        this.device = (this.device !== device && device) || "off";

        if (this.builder.layer) {
            this.builder.layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device));
        }

        if (this.preview) {
            this.preview.setDevice(this.device);
        }

        this.header.update();

        return this.device === device;
    }

    closePreview(): void {
        if (this.device !== "off") {
            this.toggleDevice(this.device);
        }
    }

    openCheatsheet(): void {
        Components.Tutorial.open(this.layer, this.style.tutorial);
    }
}
