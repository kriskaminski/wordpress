export const HELP = "https://tripetto.com/help/wordpress/";
export const HELP_RUNNERS = "https://tripetto.com/help/articles/how-to-switch-between-form-faces/";
export const HELP_STYLES = "https://tripetto.com/help/articles/how-to-style-your-forms/";
export const HELP_L10N = "https://tripetto.com/help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/";
export const HELP_PREVIEW = "https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/";
export const HELP_NOTIFICATION = "https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/";
export const HELP_SLACK = "https://tripetto.com/help/articles//help/articles/how-to-automate-email-notifications-for-each-new-result/";
export const HELP_WEBHOOK = "https://tripetto.com/help/articles/how-to-automate-a-webhook-for-each-new-result-using-zapier/";
export const HELP_GDPR = "https://tripetto.com/blog/dont-trust-someone-else-with-your-form-data/";

export const HELP_SHARE = "https://tripetto.com/help/articles/how-to-run-your-form-from-the-wordpress-plugin/";
export const HELP_LINK = "https://tripetto.com/help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/";
export const HELP_SHORTCODE = "https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site/";

export const EXAMPLES = "https://tripetto.com/examples/";
export const TUTORIALS = "https://tripetto.com/tutorials/";
export const ISSUES = "https://gitlab.com/tripetto/wordpress/issues";
export const SUBSCRIBE = "https://tripetto.com/subscribe/";
export const TWITTER = "https://twitter.com/tripetto";
export const CONTACT = "https://tripetto.com/contact/";
export const SOURCE = "https://gitlab.com/tripetto/wordpress";
