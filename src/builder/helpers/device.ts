export type DeviceSize = "xs" | "s" | "m" | "l" | "xl";

function widthToDeviceSize(width: number): DeviceSize {
    if (width < 576) {
        return "xs";
    } else if (width < 768) {
        return "s";
    } else if (width < 992) {
        return "m";
    } else if (width < 1200) {
        return "l";
    } else {
        return "xl";
    }
}

/** Returns the device size based on the screen width. */
export function deviceSize(): DeviceSize {
    return widthToDeviceSize(window.screen.width);
}

/** Returns the device size based on the window width. */
export function windowSize(): DeviceSize {
    return widthToDeviceSize(window.innerWidth);
}
