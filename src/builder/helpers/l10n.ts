import { L10n } from "tripetto-runner-foundation";
import * as Superagent from "superagent";

const localeCache: {
    [locale: string]: L10n.ILocale | undefined;
} = {};

const translationCache: {
    [language: string]: L10n.TTranslation | L10n.TTranslation[] | undefined;
} = {};

export const getLocale = (ajaxUrl: string, query?: "auto" | string) =>
    new Promise<L10n.ILocale | undefined>((resolve: (locale: L10n.ILocale | undefined) => void) => {
        const locale = query && query !== "auto" ? query : "";
        const cache = localeCache[locale];

        if (!cache) {
            Superagent.post(ajaxUrl)
                .type("form")
                .send({
                    action: "tripetto_locale",
                    locale,
                })
                .then((response: Superagent.Response) => resolve((localeCache[locale] = response.body || undefined)))
                .catch(() => resolve(undefined));
        } else {
            resolve(cache);
        }
    });

export const getTranslation = (ajaxUrl: string, language?: string | "auto", context?: string) =>
    new Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>(
        (resolve: (translation: L10n.TTranslation | L10n.TTranslation[] | undefined) => void) => {
            const key = `${language && language !== "auto" ? language : ""}:${context || "*"}`;
            const cache = translationCache[key];

            if (!cache) {
                Superagent.post(ajaxUrl)
                    .type("form")
                    .send({
                        action: "tripetto_translation",
                        language: language && language !== "auto" ? language : "",
                        context,
                    })
                    .then((response) => {
                        resolve((translationCache[key] = response.body || undefined));
                    })
                    .catch(() => resolve(undefined));
            } else {
                resolve(cache);
            }
        }
    );
