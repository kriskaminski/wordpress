import { Application, Fonts, Layers, linearicons } from "tripetto";
import { BuilderComponent } from "./components/builder";
import { IBuilderProps } from "./props";
import { Locale } from "./helpers/locale";
import { Translations } from "./helpers/translations";
import { SKIN } from "./skins/default";

export function bootstrap(props: IBuilderProps): void {
    Application.run({
        /** First try to load the required external resources (fonts, locale data, etc.). */
        first: () => {
            Locale.load(props.ajaxUrl, () => Application.observe());
            Translations.load(props.ajaxUrl, () => Application.observe(), props.language || "en");

            return Fonts.load(
                [linearicons(`${props.baseUrl}/fonts/linearicons/`), ...(SKIN.fonts ? SKIN.fonts(`${props.baseUrl}/fonts/`) : [])],
                (bSucceeded: boolean) => {
                    if (!bSucceeded) {
                        console.error(`Not all fonts could be loaded!`);
                    }

                    Application.observe();
                }
            );
        },

        /** Verify if we are ready to start the application. */
        when: () => Locale.isLoaded && Translations.isLoaded && Fonts.isReady,

        /** Start the application when we are ready for it. */
        do: () =>
            Layers.Layer.app.component(new BuilderComponent(SKIN, props)).layer.hook("OnBeforeShow", "synchronous", () => {
                window.parent.postMessage(
                    {
                        type: "ready",
                    },
                    window.location.origin
                );
            }),

        /** Enable some general styles that allows the application to act more like a native one. */
        desktopClass: true,
    });
}
