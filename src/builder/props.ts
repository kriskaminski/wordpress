import { IDefinition } from "tripetto";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { IHooks } from "./components/automate/hooks";
import { IShortcode } from "./components/share/shortcode";

export interface IBuilderProps {
    readonly id: number;
    readonly baseUrl: string;
    readonly ajaxUrl: string;
    readonly shareUrl: string;
    readonly version: string;
    readonly language: string;
    readonly runner: "autoscroll";
    readonly definition?: IDefinition;
    readonly styles?: TStyles;
    readonly l10n?: TL10n;
    readonly hooks?: IHooks;
    readonly shortcode?: IShortcode;
    readonly tier?: "premium" | "standard";
    readonly open?: "share" | "styles" | "l10n" | "automate";
}
